package com.feedback.customer.vuefeedbackapplication.category;

import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.VehicleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Long> {

    @Query("from Category c where c.categoryName=?1")
    Category findCategoryByCategoryName(String name);

    @Query("from Category c where c.id=?1")
    Category findCategoryById(Long id);

    List<Category> findByStatus(Status status);

    @Query("select c from Category c where c.categoryName=?1 and c.status=?2")
    Category findByCategoryNameAndStatus(String name, Status status);

    Long countByVehicleTypeAndStatus(VehicleType vehicleType, Status status);

}
