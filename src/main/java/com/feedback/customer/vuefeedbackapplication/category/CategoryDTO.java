package com.feedback.customer.vuefeedbackapplication.category;



import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.VehicleType;

import java.io.Serializable;

public class CategoryDTO implements Serializable {

    private Long id;

    private String categoryName;

    private String categoryNameNepali;

    private String rate;

    private String categoryDescription;

    private String categoryDescriptionNepali;

    private VehicleType vehicleType;

    private Status status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryNameNepali() {
        return categoryNameNepali;
    }

    public void setCategoryNameNepali(String categoryNameNepali) {
        this.categoryNameNepali = categoryNameNepali;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public String getCategoryDescriptionNepali() {
        return categoryDescriptionNepali;
    }

    public void setCategoryDescriptionNepali(String categoryDescriptionNepali) {
        this.categoryDescriptionNepali = categoryDescriptionNepali;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }
}
