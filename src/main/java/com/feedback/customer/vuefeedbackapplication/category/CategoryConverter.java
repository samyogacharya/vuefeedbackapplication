package com.feedback.customer.vuefeedbackapplication.category;


import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.utility.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryConverter implements EntityMapper<CategoryDTO, Category> {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Category toEntity(CategoryDTO dto) {
        if (dto == null) {
            return null;
        }
        Category entity;
        if (dto.getId() != null) {
            entity = categoryRepository.findCategoryById(dto.getId());
        } else {
            entity = new Category();
            toEntity(dto, entity);
            entity.setStatus(Status.ACTIVE);
        }
        return entity;
    }

    @Override
    public Category toEntity(CategoryDTO dto, Category entity) {
        if (entity == null || dto == null) {
            return null;
        }
        entity.setCategoryName(dto.getCategoryName());
        entity.setCategoryDescription(dto.getCategoryDescription());
        entity.setCategoryNameNepali(dto.getCategoryNameNepali());
        entity.setCategoryDescriptionNepali(dto.getCategoryDescriptionNepali());
        entity.setRate(dto.getRate());
        entity.setVehicleType(dto.getVehicleType());
        return entity;
    }

    @Override
    public CategoryDTO toDto(Category entity) {
        if (entity == null) {
            return null;
        }
        CategoryDTO dto = new CategoryDTO();
        dto.setId(entity.getId());
        dto.setCategoryName(entity.getCategoryName());
        dto.setCategoryDescription(entity.getCategoryDescription());
        dto.setCategoryNameNepali(entity.getCategoryNameNepali());
        dto.setCategoryDescriptionNepali(entity.getCategoryDescriptionNepali());
        dto.setRate(entity.getRate());
        dto.setVehicleType(entity.getVehicleType());
        dto.setStatus(entity.getStatus());
        return dto;
    }

    @Override
    public List<Category> toEntity(List<CategoryDTO> dtoList) {
        if (dtoList == null || dtoList.isEmpty()) {
            return null;
        }
        return dtoList.parallelStream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<CategoryDTO> toDto(List<Category> entityList) {
        if (entityList == null) {
            return null;
        }
        return entityList.parallelStream().map(this::toDto).collect(Collectors.toList());
    }
}
