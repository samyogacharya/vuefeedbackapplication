package com.feedback.customer.vuefeedbackapplication.category;

import com.feedback.customer.vuefeedbackapplication.user.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CategoryValidator {

    @Autowired
    CategoryService categoryService;

    public CategoryError categoryError(CategoryDTO category,String type) {
        CategoryError categoryError = new CategoryError();
        categoryError.setValid(true);

        if (category.getCategoryName() == null ||
                category.getCategoryName().replaceAll(" ", "").equals("") ||
                category.getCategoryName().equals("") ||
                category.getCategoryName().trim().equals("")) {
            categoryError.setValid(Boolean.FALSE);
            categoryError.setCategoryName("Category is Required.");
        } if (category.getCategoryNameNepali() == null ||
                category.getCategoryNameNepali().replaceAll(" ", "").equals("") ||
                category.getCategoryNameNepali().equals("") ||
                category.getCategoryNameNepali().trim().equals("")) {
            categoryError.setValid(Boolean.FALSE);
            categoryError.setCategoryNameNepali("Category Nepali is Required.");
        }

        if (category.getRate() == null ||
                category.getRate().replaceAll(" ", "").equals("") ||
                category.getRate().equals("") ||
                category.getRate().trim().equals("")) {
            categoryError.setValid(Boolean.FALSE);
            categoryError.setRate("Rate is Required.");
        }

        if (category.getVehicleType()==null||
                category.getVehicleType().equals("")){
            categoryError.setValid(Boolean.FALSE);
            categoryError.setVehicleType("Invalid Vehicle Type");
        }

        if (category.getCategoryDescription() == null ||
                category.getCategoryDescription().replaceAll(" ", "").equals("") ||
                category.getCategoryDescription().equals("") ||
                category.getCategoryDescription().trim().equals("")) {
            categoryError.setValid(Boolean.FALSE);
            categoryError.setCategoryDescription("Category Description is Required.");
        } if (category.getCategoryDescriptionNepali() == null ||
                category.getCategoryDescriptionNepali().replaceAll(" ", "").equals("") ||
                category.getCategoryDescriptionNepali().equals("") ||
                category.getCategoryDescriptionNepali().trim().equals("")) {
            categoryError.setValid(Boolean.FALSE);
            categoryError.setCategoryDescriptionNepali("Category Description Nepali is Required.");
        }
        if(type.equalsIgnoreCase("edit")){
            if(!categoryService.findCategoryById(category.getId()).getCategoryName()
                    .equalsIgnoreCase(category.getCategoryName())){
                if (categoryService.findByCategoryNameAndStatus(category.getCategoryName(), Status.ACTIVE) != null) {
                    categoryError.setValid(Boolean.FALSE);
                    categoryError.setCategoryName("Category Already Exists!");
                }
            }
        }
        if (type.equalsIgnoreCase("add")) {
            if (categoryService.findByCategoryNameAndStatus(category.getCategoryName(), Status.ACTIVE) != null) {
                categoryError.setValid(Boolean.FALSE);
                categoryError.setCategoryName("Category Already Exists!");
            }
        }
        return categoryError;
    }

}
