package com.feedback.customer.vuefeedbackapplication.category;


import com.feedback.customer.vuefeedbackapplication.abstractclass.AbstractClass;
import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.VehicleType;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "category")
public class Category extends AbstractClass<Long> {

    private String categoryName;

    private String categoryNameNepali;

    private String rate;

    private String categoryDescription;

    private String categoryDescriptionNepali;

    private VehicleType vehicleType;

    private Status status;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public String getCategoryNameNepali() {
        return categoryNameNepali;
    }

    public void setCategoryNameNepali(String categoryNameNepali) {
        this.categoryNameNepali = categoryNameNepali;
    }

    public String getCategoryDescriptionNepali() {
        return categoryDescriptionNepali;
    }

    public void setCategoryDescriptionNepali(String categoryDescriptionNepali) {
        this.categoryDescriptionNepali = categoryDescriptionNepali;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }
}
