package com.feedback.customer.vuefeedbackapplication.category;


import com.feedback.customer.vuefeedbackapplication.utility.ParameterConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value="/auth")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @Autowired
    CategoryValidator categoryValidator;


    @GetMapping("/admin/category/list")
    public ResponseEntity<List<CategoryDTO>> getCategoryList()
    {
        List<CategoryDTO> categoryDTOList=categoryService.findAll();
        return new ResponseEntity<>(categoryDTOList, HttpStatus.OK);
    }

    @PostMapping("/admin/category/add")
    public ResponseEntity<Map> saveCategory(@RequestBody CategoryDTO categoryDTO)
    {
        Map<String, Object> map=new HashMap<>();
        CategoryError categoryError = categoryValidator.categoryError(categoryDTO, "add");
        if(categoryError.getValid())
        {
           categoryService.save(categoryDTO);
            map.put(ParameterConstants.PARAM_MESSAGE, "Success");
            map.put(ParameterConstants.PARAM_TEXT, "Added Successfully.");
            return new ResponseEntity<>(map, HttpStatus.OK);
        }
        else
        {
            map.put("msg", "Error");
            map.put(ParameterConstants.PARAM_CATEGORY, categoryDTO);
            map.put("error", categoryError);
            return new ResponseEntity<>(map, HttpStatus.OK);
        }
    }


     @GetMapping(value="/admin/category/edit/{id}")
     public ResponseEntity<CategoryDTO> editCategory(@PathVariable("id") Long id)
     {
         CategoryDTO categoryDTO=categoryService.findCategoryById(id);
         return new ResponseEntity<>(categoryDTO, HttpStatus.OK);
     }


     @PostMapping("/admin/category/edit")
     public ResponseEntity<Map> editCategory(@RequestBody CategoryDTO categoryDTO)
     {
         Map<String, Object> map=new HashMap<>();
         CategoryError categoryError=categoryValidator.categoryError(categoryDTO, "edit");
         if(categoryError.getValid())
         {
             categoryService.edit(categoryDTO);
             map.put(ParameterConstants.PARAM_MESSAGE, "Success");
             map.put(ParameterConstants.PARAM_TEXT, "Edit Successfully.");
             return new ResponseEntity<>(map, HttpStatus.OK);
         }
         else {
             map.put("msg", "Error");
             map.put(ParameterConstants.PARAM_CATEGORY, categoryDTO);
             map.put("error", categoryError);
             return new ResponseEntity<>(map, HttpStatus.OK);
         }
     }

}
