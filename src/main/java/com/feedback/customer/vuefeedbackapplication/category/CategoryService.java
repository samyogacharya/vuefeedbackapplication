package com.feedback.customer.vuefeedbackapplication.category;


import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.VehicleType;

import java.util.List;

public interface CategoryService {

    CategoryDTO save(CategoryDTO categoryDTO);

    CategoryDTO edit(CategoryDTO categoryDTO);

    List<CategoryDTO> findAll();

    CategoryDTO findCategoryByCategoryName(String name);

    CategoryDTO findCategoryById(Long id);

    CategoryDTO findByCategoryNameAndStatus(String name, Status status);

    Long countByVehicleTypeAndStatus(VehicleType vehicleType);

}
