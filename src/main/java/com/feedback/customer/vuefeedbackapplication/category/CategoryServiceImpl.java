package com.feedback.customer.vuefeedbackapplication.category;

import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.VehicleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    private CategoryConverter categoryConverter;


    @Override
    public CategoryDTO save(CategoryDTO categoryDTO) {
        Category category = categoryConverter.toEntity(categoryDTO);
        category = categoryRepository.save(category);
        return categoryConverter.toDto(category);
    }

    @Override
    public CategoryDTO edit(CategoryDTO categoryDTO) {
        Category category = categoryConverter.toEntity(categoryDTO);
        category.setCategoryName(categoryDTO.getCategoryName());
        category.setCategoryNameNepali(categoryDTO.getCategoryNameNepali());
        category.setCategoryDescription(categoryDTO.getCategoryDescription());
        category.setCategoryDescriptionNepali(categoryDTO.getCategoryDescriptionNepali());
        category.setRate(categoryDTO.getRate());
        category.setVehicleType(categoryDTO.getVehicleType());
        category.setLastModified(new Date());
        return categoryConverter.toDto(categoryRepository.save(category));
    }

    @Override
    public List<CategoryDTO> findAll() {
        return categoryConverter.toDto(categoryRepository.findByStatus(Status.ACTIVE));
    }

    @Override
    public CategoryDTO findCategoryByCategoryName(String name) {
        return categoryConverter.toDto(categoryRepository.findCategoryByCategoryName(name));
    }

    @Override
    public CategoryDTO findCategoryById(Long id) {
        return categoryConverter.toDto(categoryRepository.findCategoryById(id));
    }

    @Override
    public CategoryDTO findByCategoryNameAndStatus(String name, Status status) {
        return categoryConverter.toDto(categoryRepository.findByCategoryNameAndStatus(name, status));
    }

    @Override
    public Long countByVehicleTypeAndStatus(VehicleType vehicleType) {
        return categoryRepository.countByVehicleTypeAndStatus(vehicleType, Status.ACTIVE);
    }

}
