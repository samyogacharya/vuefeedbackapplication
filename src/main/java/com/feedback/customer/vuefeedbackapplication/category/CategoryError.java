package com.feedback.customer.vuefeedbackapplication.category;

public class CategoryError {
    private String categoryName;

    private String categoryNameNepali;

    private String categoryDescription;

    private String categoryDescriptionNepali;

    private String rate;

    private String vehicleType;

    private Boolean valid;


    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryNameNepali() {
        return categoryNameNepali;
    }

    public void setCategoryNameNepali(String categoryNameNepali) {
        this.categoryNameNepali = categoryNameNepali;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public String getCategoryDescriptionNepali() {
        return categoryDescriptionNepali;
    }

    public void setCategoryDescriptionNepali(String categoryDescriptionNepali) {
        this.categoryDescriptionNepali = categoryDescriptionNepali;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }
}
