package com.feedback.customer.vuefeedbackapplication.customer;

import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long> {

    @Query("select t from Customer t where t.name=?1 and t.status=?2")
    Customer findByNameAndStatus(String name, Status status);

    @Query("from Customer t where t.id=?1")
    Customer findCustomerById(Long id);

    Customer findByPhoneNumberAndStatus(String phoneNumber, Status status);

    List<Customer> findByStatus(Status status);

    Customer findByUserAndStatus(User user, Status status);


}
