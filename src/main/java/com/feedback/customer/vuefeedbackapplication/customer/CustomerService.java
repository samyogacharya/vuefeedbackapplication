package com.feedback.customer.vuefeedbackapplication.customer;


import com.feedback.customer.vuefeedbackapplication.staff.BranchUserDTO;
import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.User;

import java.util.List;

public interface CustomerService {

    CustomerDTO save(CustomerDTO customerDTO);

    Customer findByNameAndStatus(String name, Status status);

    Customer findByUserAndStatus(User user, Status status);

    List<CustomerDTO> findAll();

    CustomerDTO findByPhoneNumberAndStatus(String phoneNumber);

    CustomerDTO findCustomerById(Long id);

    CustomerDTO edit(CustomerDTO customerDTO, BranchUserDTO userDTO);

}
