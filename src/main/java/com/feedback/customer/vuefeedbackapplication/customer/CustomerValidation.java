package com.feedback.customer.vuefeedbackapplication.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerValidation {

    @Autowired
    CustomerService customerService;

    public CustomerError validateCustomer(CustomerDTO customerDTO, String type) {
        CustomerError customerError = new CustomerError();
        Boolean valid = true;

        if (customerDTO.getPhoneNumber() == null ||
                customerDTO.getPhoneNumber().equals("") ||
                customerDTO.getPhoneNumber().trim().equals("") ||
                customerDTO.getPhoneNumber().replaceAll(" ", "").equals("")) {
            valid = false;
            customerError.setPhoneNumber("Invalid phone Number!");
        }

        if (type.equalsIgnoreCase("add")) {
            CustomerDTO customerDTO1 =
                    customerService.findByPhoneNumberAndStatus(customerDTO.getPhoneNumber().trim());
            if (customerDTO1 != null) {
                valid = false;
                customerError.setPhoneNumber("Phone Number already exists!");
            }
        }

        if(type.equalsIgnoreCase("edit")){
            if(!customerService.findCustomerById(customerDTO.getId()).getPhoneNumber()
                    .equalsIgnoreCase(customerDTO.getPhoneNumber())){
                if (customerService.findByPhoneNumberAndStatus(customerDTO.getPhoneNumber().trim()) != null) {
                    valid = false;
                    customerError.setPhoneNumber("Phone Number already exists!");
                }
            }
        }

        customerError.setValid(valid);
        return customerError;
    }

}
