package com.feedback.customer.vuefeedbackapplication.customer;

import com.feedback.customer.vuefeedbackapplication.configuration.SecurityUtility;
import com.feedback.customer.vuefeedbackapplication.customeranswer.CustomerAnswerDTO;
import com.feedback.customer.vuefeedbackapplication.customeranswer.CustomerAnswerService;
import com.feedback.customer.vuefeedbackapplication.staff.BranchConverter;
import com.feedback.customer.vuefeedbackapplication.staff.BranchService;
import com.feedback.customer.vuefeedbackapplication.staff.BranchUserDTO;
import com.feedback.customer.vuefeedbackapplication.user.*;
import com.feedback.customer.vuefeedbackapplication.utility.ParameterConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/auth")
public class CustomerController {

    @Autowired
    CustomerValidation customerValidation;

    @Autowired
    CustomerService customerService;

    @Autowired
    UserValidation userValidation;

    @Autowired
    SecurityUtility securityUtility;

    @Autowired
    UserService userService;

    @Autowired
    BranchConverter branchConverter;

    @Autowired
    BranchService branchService;

    @Autowired
    CustomerAnswerService customerAnswerService;


    @GetMapping("/customer/list")
    public String getCustomerList(Model model) {
        User user = securityUtility.getSecurity();
        if (user.getUserType().name().equalsIgnoreCase("SYSTEM_ADMIN")) {
            model.addAttribute(ParameterConstants.PARAM_CUSTOMER, customerService.findAll());
            model.addAttribute(ParameterConstants.PARAM_SIDEBAR, "admin");
        }
//        else {
//            model.addAttribute(ParameterConstants.PARAM_CUSTOMER, customerService.findAll());
//            model.addAttribute(ParameterConstants.PARAM_SIDEBAR, "admin");
//        }
        return "customer/list";
    }

    @GetMapping("/customer/add")
    public String getCustomerForm(Model model) {
        if (securityUtility.getSecurity().getUserType().name().equalsIgnoreCase("SYSTEM_ADMIN")) {
            return "redirect:/404";
        } else {
            model.addAttribute(ParameterConstants.PARAM_ACTION, "add");
            model.addAttribute(ParameterConstants.PARAM_ACTION, "add");
            model.addAttribute(ParameterConstants.PARAM_SHIFT_LIST, java.util.Arrays.asList(Shift.values()));
            return "customer/add";
        }
    }

    @GetMapping("/customer/edit/{id}")
    public String getEditPage(@PathVariable("id") Long id, Model model) {
        if (securityUtility.getSecurity().getUserType().name().equalsIgnoreCase("SYSTEM_ADMIN")) {
            return "redirect:/404";
        } else {
            CustomerDTO customerDTO = customerService.findCustomerById(id);
            UserDTO userDTO = userService.findUserById(customerDTO.getUserId());
            userDTO.setPassword("");
            model.addAttribute(ParameterConstants.PARAM_ACTION, "edit");
            model.addAttribute(ParameterConstants.PARAM_SHIFT_LIST, java.util.Arrays.asList(Shift.values()));
            model.addAttribute(ParameterConstants.PARAM_CUSTOMER, customerDTO);
            model.addAttribute(ParameterConstants.PARAM_USER, userDTO);
            return "customer/add";
        }
    }

    @PostMapping("/customer/edit")
    public String editCustomer(CustomerDTO customerDTO, BranchUserDTO userDTO, RedirectAttributes redirectAttributes,
                               @ModelAttribute("userId") Long uid, Model model) {
        userDTO.setId(uid);
        customerDTO.setUserId(uid);
        CustomerError customerError = customerValidation.validateCustomer(customerDTO, "edit");
        UserError userError = userValidation.validateUser(userDTO, "edit");
        if (customerError.getValid() && userError.getValid()) {
            customerService.edit(customerDTO, userDTO);
            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_MESSAGE, "Success");
            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_TEXT, "Edited Sucessfully.");
            return "redirect:/auth/customer/list";
        } else {
            model.addAttribute(ParameterConstants.PARAM_ACTION, "edit");
            model.addAttribute(ParameterConstants.PARAM_SHIFT_LIST, java.util.Arrays.asList(Shift.values()));
            model.addAttribute(ParameterConstants.PARAM_CUSTOMER, customerDTO);
            model.addAttribute(ParameterConstants.PARAM_USER, userDTO);
            model.addAttribute(ParameterConstants.PARAM_ERROR, customerError);
            model.addAttribute(ParameterConstants.PARAM_USER_ERROR, userDTO);
            return "customer/add";
        }
    }

//    @PostMapping("/customer/add")
//    public String saveCustomer(CustomerDTO customerDTO, UserDTO userDTO, RedirectAttributes redirectAttributes) {
//        CustomerError customerError = customerValidation.validateCustomer(customerDTO, "add");
//        UserError userError = userValidation.validateUser(userDTO, "add");
//        if (customerError.getValid() && userError.getValid()) {
//            customerService.save(customerDTO, userDTO);
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_MESSAGE, "Success");
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_TEXT, "Added Sucessfully.");
//            return "redirect:/auth/customer/list";
//        } else {
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_MESSAGE, "Warning");
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_ERROR, customerError);
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_USER_ERROR, userError);
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_CUSTOMER, customerDTO);
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_USER, userDTO);
//            return "redirect:/auth/customer/add";
//        }
//    }

//    @GetMapping("/admin/tellerlist/{id}")
//    public String getTellerReport(@PathVariable("id") Long id, Model model) {
//        BranchDTO branchDTO = branchService.findBranchById(id);
//        model.addAttribute(ParameterConstants.PARAM_CUSTOMER, customerService.findByBranch(branchDTO));
//        return "branch/tellerlist";
//    }

    @GetMapping("/customerlist/report/{id}")
    public String getBranchReport(@PathVariable("id") Long id, Model model) {
        User user = securityUtility.getSecurity();
        if (user.getUserType().name().equalsIgnoreCase("SYSTEM_ADMIN")) {
            model.addAttribute(ParameterConstants.PARAM_SIDEBAR, "admin");
        } else if (user.getUserType().name().equalsIgnoreCase("ADMIN")) {
            model.addAttribute(ParameterConstants.PARAM_SIDEBAR, "branch");
        } else {
            model.addAttribute(ParameterConstants.PARAM_SIDEBAR, "customer");
        }
        CustomerDTO customerDTO = customerService.findCustomerById(id);
        List<CustomerAnswerDTO> answerList = customerAnswerService.findByCustomer(customerDTO);
        List<CustomerAnswerDTO> newList = new ArrayList<>();

        Map<String, CustomerAnswerDTO> stringStringMap = new HashMap<>();
//        for (CustomerAnswerDTO customerAnswerDTO : newList) {
//            stringStringMap.put(customerAnswerDTO.getAnswerName(), customerAnswerDTO);
//        }
        model.addAttribute(ParameterConstants.PARAM_CUSTOMER_ANSWER, stringStringMap);
        model.addAttribute("tellerName", customerDTO.getName());

        return "customeranswer/list";
    }


    @GetMapping("/admin/branchreport")
    public String dashboard(Model model){
        model.addAttribute(ParameterConstants.PARAM_USER,userService.findAll().size());
        model.addAttribute(ParameterConstants.PARAM_BRANCH,branchService.findAll());
        model.addAttribute(ParameterConstants.PARAM_CUSTOMER, customerService.findAll().size());
        return "branch/index";
    }

}
