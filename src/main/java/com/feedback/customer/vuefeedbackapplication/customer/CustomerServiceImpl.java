package com.feedback.customer.vuefeedbackapplication.customer;

import com.feedback.customer.vuefeedbackapplication.configuration.SecurityUtility;
import com.feedback.customer.vuefeedbackapplication.staff.BranchConverter;
import com.feedback.customer.vuefeedbackapplication.staff.BranchUserDTO;
import com.feedback.customer.vuefeedbackapplication.user.*;
import com.feedback.customer.vuefeedbackapplication.utility.HashPassword;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerConverter customerConverter;

    @Autowired
    SecurityUtility securityUtility;

    @Autowired
    BranchConverter branchConverter;

    @Autowired
    HashPassword hashPassword;

    @Autowired
    UserConverter userConverter;

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Override
    public CustomerDTO save(CustomerDTO customerDTO) {
        UserDTO userDTO=new UserDTO();
        userDTO.setEmail(customerDTO.getEmail());
        userDTO.setUserType(UserType.CUSTOMER);
        userDTO.setStatus(Status.ACTIVE);
        userDTO.setPassword(customerDTO.getPassword());
        userDTO.setUsername(customerDTO.getPhoneNumber());
        User user = userConverter.toEntity(userService.save(userDTO));
        Customer customer = customerConverter.toEntity(customerDTO);
        customer.setStatus(Status.ACTIVE);
        customer.setUser(user);
        return customerConverter.toDto(customerRepository.save(customer));
    }

    @Override
    public Customer findByNameAndStatus(String name, Status status) {
        return customerRepository.findByNameAndStatus(name, status);
    }

    @Override
    public Customer findByUserAndStatus(User user, Status status) {
        return customerRepository.findByUserAndStatus(user, status);
    }

    @Override
    public List<CustomerDTO> findAll() {
        return customerConverter.toDto(customerRepository.findByStatus(Status.ACTIVE));
    }

    @Override
    public CustomerDTO findByPhoneNumberAndStatus(String phoneNumber) {
        return customerConverter.toDto(customerRepository.findByPhoneNumberAndStatus(phoneNumber,Status.ACTIVE));
    }

    @Override
    public CustomerDTO findCustomerById(Long id) {
        return customerConverter.toDto(customerRepository.findCustomerById(id));
    }

    @Override
    public CustomerDTO edit(CustomerDTO customerDTO, BranchUserDTO userDTO) {
        Customer customer = customerConverter.toEntity(customerDTO);
        customer.setName(customerDTO.getName());
        customer.setAddress(customerDTO.getAddress());
        customer.setPhoneNumber(customerDTO.getPhoneNumber());
        customer.setEmail(customerDTO.getEmail());

        User user = customer.getUser();
        user.setEmail(userDTO.getEmail());
        user.setUsername(userDTO.getUsername());
        user.setPassword(hashPassword.hashPassword(userDTO.getPassword()));
        customer.setUser(userRepository.save(user));

        return customerConverter.toDto(customerRepository.save(customer));
    }

}
