package com.feedback.customer.vuefeedbackapplication.customer;

import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.UserRepository;
import com.feedback.customer.vuefeedbackapplication.utility.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerConverter implements EntityMapper<CustomerDTO, Customer> {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public Customer toEntity(CustomerDTO dto) {
        if (dto == null) {
            return null;
        }
        Customer entity;
        if (dto.getId() != null) {
            entity = customerRepository.findCustomerById(dto.getId());
        } else {
            entity = new Customer();
            toEntity(dto, entity);
            entity.setStatus(Status.ACTIVE);
        }
        return entity;
    }

    @Override
    public Customer toEntity(CustomerDTO dto, Customer entity) {
        if (entity == null || dto == null) {
            return null;
        }
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setAddress(dto.getAddress());
        entity.setName(dto.getName());
        entity.setEmail(dto.getEmail());
        entity.setUser(userRepository.findUserById(dto.getId()));
        return entity;
    }

    @Override
    public CustomerDTO toDto(Customer entity) {
        if (entity == null) {
            return null;
        }
        CustomerDTO dto = new CustomerDTO();
        dto.setId(entity.getId());
        dto.setStatus(entity.getStatus());
        dto.setName(entity.getName());
        dto.setAddress(entity.getAddress());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setEmail(entity.getEmail());
        dto.setUserId(entity.getUser().getId());
        dto.setUsername(entity.getUser().getUsername());
        return dto;
    }

    @Override
    public List<Customer> toEntity(List<CustomerDTO> dtoList) {
        if (dtoList == null || dtoList.isEmpty()) {
            return null;
        }
        return dtoList.parallelStream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<CustomerDTO> toDto(List<Customer> entityList) {
        if (entityList == null) {
            return null;
        }
        return entityList.parallelStream().map(this::toDto).collect(Collectors.toList());
    }
}
