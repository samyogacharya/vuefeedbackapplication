package com.feedback.customer.vuefeedbackapplication.controller.rest;

import com.feedback.customer.vuefeedbackapplication.answer.AnswerDTO;
import com.feedback.customer.vuefeedbackapplication.answer.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class AnswerResource {

    @Autowired
    AnswerService answerService;

    @Autowired
    ResponseDetail responseDetail;

    @GetMapping("/answer")
    public ResponseEntity<ResponseDTO> getAnswer() {
        ResponseDTO responseDTO;
        List<AnswerDTO> listAnswer = answerService.findAll();
        if (listAnswer != null) {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully retrieved Answer list", listAnswer);
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        } else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.BAD_REQUEST, "Unable to retrieve Answer list", null);
            return new ResponseEntity<>(responseDTO, HttpStatus.BAD_REQUEST);
        }
    }

}
