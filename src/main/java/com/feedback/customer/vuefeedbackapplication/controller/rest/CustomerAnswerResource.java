package com.feedback.customer.vuefeedbackapplication.controller.rest;


import com.feedback.customer.vuefeedbackapplication.answer.AnswerError;
import com.feedback.customer.vuefeedbackapplication.answer.AnswerService;
import com.feedback.customer.vuefeedbackapplication.answer.AnswerValidation;
import com.feedback.customer.vuefeedbackapplication.configuration.SecurityUtility;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerConverter;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerDTO;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerService;
import com.feedback.customer.vuefeedbackapplication.customeranswer.CustomerAnswerDTO;
import com.feedback.customer.vuefeedbackapplication.customeranswer.CustomerAnswerResponseDTO;
import com.feedback.customer.vuefeedbackapplication.customeranswer.CustomerAnswerService;
import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.User;
import com.feedback.customer.vuefeedbackapplication.user.UserService;
import com.feedback.customer.vuefeedbackapplication.utility.FileUploadService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class CustomerAnswerResource {

    @Autowired
    CustomerAnswerService customerAnswerService;

    @Autowired
    CustomerService customerService;

    @Autowired
    AnswerService answerService;

    @Autowired
    ResponseDetail responseDetail;

    @Autowired
    SecurityUtility securityUtility;

    @Autowired
    CustomerConverter customerConverter;

    @Autowired
    UserService userService;

    @Autowired
    FileUploadService fileUploadService;

    @Autowired
    AnswerValidation answerValidation;

    @PostMapping("/customeranswer")
    public ResponseEntity<ResponseDTO> saveCustomerAnswer(@RequestBody CustomerAnswerDTO customerAnswerDTO) throws IOException {
        ResponseDTO responseDTO;
        User user = securityUtility.getSecurity();
        CustomerDTO customerDTO = customerConverter.toDto(customerService.findByUserAndStatus(userService.findByUserName(user.getUsername()), Status.ACTIVE));
        customerAnswerDTO.setCustomerId(customerDTO.getId());
        customerAnswerDTO=customerAnswerService.save(customerAnswerDTO);
        responseDTO = responseDetail.getResponseDTOforImage(ResponseStatus.SUCCESS, "Successfully Saved",
                customerAnswerDTO.getId());
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }

    @PostMapping("/customeranswer/{id}/document")
    public ResponseEntity<ResponseDTO> uploadImages(@RequestParam(value="file",required = false) MultipartFile[] file,
                                                    @PathVariable Long id) throws IOException {
        ResponseDTO responseDTO;
        CustomerAnswerDTO customerAnswerDTO = customerAnswerService.findById(id);
        List<String> answerImage = new ArrayList<>();
        for (MultipartFile multipartFile:file){
            String fileName=customerAnswerService.fileName(multipartFile);
            answerImage.add(fileName);
        }
        customerAnswerDTO.setImages(answerImage);
        customerAnswerService.saveImages(customerAnswerDTO);
        responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully uploaded", null);
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }

    @PostMapping("/customerremaks")
    public ResponseEntity<ResponseDTO> saveCustomerRemarks(@RequestBody CustomerAnswerDTO customerAnswerDTO){
        ResponseDTO responseDTO;
        CustomerAnswerDTO customerAnswerDTO1 = customerAnswerService.findById(customerAnswerDTO.getId());
        customerAnswerDTO.setAnswerId(customerAnswerDTO1.getAnswerId());
        AnswerError answerError = answerValidation.validateAnswer(customerAnswerDTO);
        if (answerError.getValid()) {
            answerService.customerRemarks(customerAnswerDTO);
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully Saved", null);
        }else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.FAILURE, answerError.getComments(), null);
        }
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }

    @GetMapping("/customeranswer/history")
    public ResponseEntity<ResponseDTO> getCustomerAnswerByCustomer(){
        ResponseDTO responseDTO;
        User user = securityUtility.getSecurity();
        CustomerDTO customerDTO = customerConverter.toDto(customerService.findByUserAndStatus(userService.findByUserName(user.getUsername()), Status.ACTIVE));
        List<CustomerAnswerDTO> customerAnswerDTOList =
                customerAnswerService.findByCustomer(customerService.findCustomerById(customerDTO.getId()));
        responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully retrieved",
                convertToResponseList(customerAnswerDTOList));
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }

    public List<CustomerAnswerResponseDTO> convertToResponseList(List<CustomerAnswerDTO> customerAnswerDTO){
        List<CustomerAnswerResponseDTO> customerAnswerResponseDTOS = new ArrayList<>();
        for (CustomerAnswerDTO customerAnswer :
                customerAnswerDTO) {
            CustomerAnswerResponseDTO customerAnswerResponseDTO = new CustomerAnswerResponseDTO();
            customerAnswerResponseDTO.setBranch(customerAnswer.getBranchName());
            customerAnswerResponseDTO.setCategory(customerAnswer.getCategoryName());
            customerAnswerResponseDTO.setComments(customerAnswerService.convetToComment(customerAnswer));
            customerAnswerResponseDTO.setImages(customerAnswer.getImages());
            customerAnswerResponseDTO.setId(customerAnswer.getId());
            customerAnswerResponseDTO.setTitle(customerAnswer.getAnswerTitle());
            customerAnswerResponseDTO.setDescription(customerAnswer.getAnswerDescription());

            DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm a");
            String strDate = dateFormat.format(customerAnswer.getInsertDate());
            customerAnswerResponseDTO.setCreatedDate(strDate);

            customerAnswerResponseDTOS.add(customerAnswerResponseDTO);
        }
        customerAnswerResponseDTOS.sort(Comparator.comparing(CustomerAnswerResponseDTO::getId).reversed());
        return customerAnswerResponseDTOS;
    }

    @RequestMapping(value = "/view/image", method = RequestMethod.GET)
    public void viewProjectTypeImage(@RequestParam("fileName") String fileName, HttpServletResponse response, HttpServletRequest request) throws IOException {
        if (!StringUtils.isEmpty(fileName)) {
            fileName = URLDecoder.decode(fileName, "UTF-8");

            File imagePath = new File(fileUploadService.getBasePath()+"complaints/"+fileName);
            if (imagePath.exists()) {
                String ext = FilenameUtils.getExtension(fileName);
                if (ext.equals("png") || ext.equals("jpg") || ext.equals("jpeg")) {
                    response.setContentType("image/" + ext);
                } else if (ext.equals("pdf")) {
                    response.setContentType("application/" + ext);
                }
                response.setHeader("Content-Disposition", "attachment;filename=" + fileName);

                FileCopyUtils.copy(new FileInputStream(imagePath), response.getOutputStream());

            }

        }

    }


}