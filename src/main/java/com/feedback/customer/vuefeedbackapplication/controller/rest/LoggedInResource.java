package com.feedback.customer.vuefeedbackapplication.controller.rest;

import com.feedback.customer.vuefeedbackapplication.configuration.SecurityUtility;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerConverter;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerDTO;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerService;
import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.User;
import com.feedback.customer.vuefeedbackapplication.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class LoggedInResource {

    @Autowired
    private SecurityUtility securityUtility;

    @Autowired
    private CustomerConverter customerConverter;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private UserService userService;

    @Autowired
    ResponseDetail responseDetail;

    @GetMapping("/profile")
    public ResponseEntity<ResponseDTO> getTellerProfile() {
        User user = securityUtility.getSecurity();
        ResponseDTO responseDTO;
        CustomerDTO customerDTO = customerConverter.toDto(customerService.findByUserAndStatus(userService.findByUserName(user.getUsername()), Status.ACTIVE));
        if (customerDTO != null) {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully retrieved Customer.", customerDTO);
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        } else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.BAD_REQUEST, "Unable to retrieve Customer.", null);
            return new ResponseEntity<>(responseDTO, HttpStatus.BAD_REQUEST);
        }
    }
}
