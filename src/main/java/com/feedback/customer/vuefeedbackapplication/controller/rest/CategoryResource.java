package com.feedback.customer.vuefeedbackapplication.controller.rest;

import com.feedback.customer.vuefeedbackapplication.category.CategoryDTO;
import com.feedback.customer.vuefeedbackapplication.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class CategoryResource {

    @Autowired
    CategoryService categoryService;

    @Autowired
    ResponseDetail responseDetail;

    @GetMapping("/category")
    public ResponseEntity<ResponseDTO> getCategory() {
        ResponseDTO responseDTO;
        List<CategoryDTO> list = categoryService.findAll();
        if (list != null) {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully retrieved Category list", list);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
        } else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.BAD_REQUEST, "Unable to retrieve Category list", null);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.BAD_REQUEST);
        }
    }
}
