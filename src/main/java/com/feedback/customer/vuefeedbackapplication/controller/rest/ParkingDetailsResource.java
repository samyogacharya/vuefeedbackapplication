package com.feedback.customer.vuefeedbackapplication.controller.rest;

import com.feedback.customer.vuefeedbackapplication.configuration.SecurityUtility;
import com.feedback.customer.vuefeedbackapplication.parkingdetails.ParkingDetailsDTO;
import com.feedback.customer.vuefeedbackapplication.parkingdetails.ParkingDetailsService;
import com.feedback.customer.vuefeedbackapplication.staff.Branch;
import com.feedback.customer.vuefeedbackapplication.staff.BranchConverter;
import com.feedback.customer.vuefeedbackapplication.staff.BranchService;
import com.feedback.customer.vuefeedbackapplication.user.User;
import com.feedback.customer.vuefeedbackapplication.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

@RestController
@RequestMapping(value = "/api")
public class ParkingDetailsResource {

    @Autowired
    ParkingDetailsService parkingDetailsService;

    @Autowired
    ResponseDetail responseDetail;

    @Autowired
    SecurityUtility securityUtility;

    @Autowired
    BranchService branchService;

    @Autowired
    BranchConverter branchConverter;

    @Autowired
    UserService userService;

    @PostMapping("/parkingdetails/checkin")
    public ResponseEntity<ResponseDTO> saveCheckIn(@RequestBody ParkingDetailsDTO parkingDetailsDTO) {
        ResponseDTO responseDTO = null;
        User user = userService.findByUserName(securityUtility.getSecurity().getUsername());
        Branch branch = branchConverter.toEntity(branchService.findByUserAndStatus(user));
        parkingDetailsDTO.setStaffId(branch.getId());
        parkingDetailsDTO.setCheckIn(String.valueOf(LocalDateTime.now()));
        parkingDetailsService.save(parkingDetailsDTO);
        responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS,"Checked In", null);
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }

    @PostMapping("/parkingdetails/checkout")
    public ResponseEntity<ResponseDTO> saveCheckOut(@RequestBody ParkingDetailsDTO parkingDetailsDTO) {
        ResponseDTO responseDTO = null;
        if (parkingDetailsDTO.getId()==null){
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.FAILURE,"Check in first", null);
        }else {
            ParkingDetailsDTO dto = parkingDetailsService.findOne(parkingDetailsDTO.getId());
            dto.setCheckOut(String.valueOf(LocalDateTime.now()));
            dto=parkingDetailsService.save(dto);

            LocalTime checkIn= LocalTime.parse(dto.getCheckIn(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            LocalTime checkout= LocalTime.parse(dto.getCheckOut(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);

            Double diffTime = Double.valueOf(ChronoUnit.MINUTES.between(checkIn,checkout));

            Double rate = Double.valueOf(dto.getRate());
            Double priceInMin = rate / 60;

            Double price = diffTime * priceInMin;

            Double finalPrice = 5*(Math.ceil(Math.abs(price/5)));

            dto.setDuration(diffTime +" Minutes");
            dto.setPrice(finalPrice);

            parkingDetailsService.save(dto);

            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Checked Out", null);
        }
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }
}
