package com.feedback.customer.vuefeedbackapplication.controller.rest;

import org.springframework.stereotype.Component;

@Component
public class ResponseDetail {

    public ResponseDTO getResponseDTO(ResponseStatus status, String message, Object detail) {
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setStatus(status.getKey());
        responseDTO.setCode(status.getValue());
        responseDTO.setMessage(message);
        responseDTO.setDetails(detail);
        return responseDTO;
    }

    public ResponseDTO getResponseDTOforImage(ResponseStatus status, String message, Long id) {
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setStatus(status.getKey());
        responseDTO.setCode(status.getValue());
        responseDTO.setMessage(message);
        responseDTO.setId(id);
        return responseDTO;
    }


}
