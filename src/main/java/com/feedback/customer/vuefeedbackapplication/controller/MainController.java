package com.feedback.customer.vuefeedbackapplication.controller;

import com.feedback.customer.vuefeedbackapplication.configuration.SecurityUtility;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerService;
import com.feedback.customer.vuefeedbackapplication.parkingdetails.ParkingDetailsService;
import com.feedback.customer.vuefeedbackapplication.staff.BranchConverter;
import com.feedback.customer.vuefeedbackapplication.staff.BranchDTO;
import com.feedback.customer.vuefeedbackapplication.staff.BranchService;
import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.User;
import com.feedback.customer.vuefeedbackapplication.user.UserService;
import com.feedback.customer.vuefeedbackapplication.user.VehicleType;
import com.feedback.customer.vuefeedbackapplication.utility.ParameterConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

@RestController
@RequestMapping(value="/auth")
public class MainController {

    @Autowired
    SecurityUtility securityUtility;

    @Autowired
    CustomerService customerService;

    @Autowired
    BranchConverter branchConverter;


    @Autowired
    UserService userService;

    @Autowired
    BranchService branchService;

    @Autowired
    ParkingDetailsService parkingDetailsService;

    @GetMapping("/")
    public ResponseEntity<Map> home()
    {
        Map<String, Object> map=new HashMap<>();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name=auth.getName();
        User user=userService.findByUserName(name);
        if(user.getUserType().name().equalsIgnoreCase("SYSTEM_ADMIN")) {
            map.put("userId", user.getId());
            map.put("userType", user.getUserType());
            return new ResponseEntity<>(map, HttpStatus.OK);
        }
        else if (user.getUserType().name().equalsIgnoreCase("ADMIN")) {
            map.put("userId", user.getId());
            map.put("userType", user.getUserType());
            return new ResponseEntity<>(map, HttpStatus.OK);
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }


    @RequestMapping("/logout-success")
    public String logoutSuccess()
    {
        System.out.println("logout is called");
        return "logout.html";
    }



    @RequestMapping(path="/admin/dashboard", method= RequestMethod.GET)
    public List<Map> DashBoard() {
        List<Map> mapList = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put(ParameterConstants.PARAM_USER, userService.findAll().size());
        map.put(ParameterConstants.PARAM_BRANCH, branchService.findAll());
        map.put("numberOfStaff", branchService.countBranchByStatus(Status.ACTIVE));
        map.put("totalTransaction", parkingDetailsService.countTotalTransaction());
        map.put("totalAmount", parkingDetailsService.findTotalPrice());
        map.put("two", parkingDetailsService.countByCategory_VehicleType(VehicleType.TWO_WHEELER));
        map.put("four", parkingDetailsService.countByCategory_VehicleType(VehicleType.FOUR_WHEELER));
        map.put("six", parkingDetailsService.countByCategory_VehicleType(VehicleType.SIX_WHEELER));
        mapList.add(map);
        return mapList;
    }


    @GetMapping("/dashboard")
    public ResponseEntity<List<Map>> getBranchDashboard() throws ParseException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name=auth.getName();
        User user=userService.findByUserName(name);
        List<Map> mapList = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        BranchDTO branchDTO = branchService.findBranchesByUserId(user.getId());
        System.out.println(parkingDetailsService.findTotalPriceByBranchId(branchDTO.getId()));
        LocalDate currentDate = LocalDate.from((LocalDate.now().plusDays(1)));     //Today
        LocalDate asYesterday = currentDate.minusDays(1);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date today = formatter.parse(String.valueOf(currentDate));
        Date yesterday = formatter.parse(String.valueOf(asYesterday));
        Integer todayTotalTransaction=parkingDetailsService.countTotalTransactionByBranchIdAndCreatedBetween(branchDTO.getId(), today, yesterday);
        map.put(ParameterConstants.PARAM_USER, userService.findAll().size());
        map.put(ParameterConstants.PARAM_BRANCH, branchService.findBranchesByUserId(user.getId()));
        map.put("totalTransaction",parkingDetailsService.countTotalTransactionByBranchId(branchDTO.getId()));
        map.put("totalAmount", parkingDetailsService.findTotalPriceByBranchId(branchDTO.getId()));
        map.put("two",parkingDetailsService.countByCategoryVehicleTypeAndBranchId((VehicleType.TWO_WHEELER), branchDTO.getId()));
        map.put("four",parkingDetailsService.countByCategoryVehicleTypeAndBranchId((VehicleType.FOUR_WHEELER), branchDTO.getId()));
        map.put("todayTotalTransaction", todayTotalTransaction);
        map.put("six",parkingDetailsService.countByCategoryVehicleTypeAndBranchId((VehicleType.SIX_WHEELER), branchDTO.getId()));
        mapList.add(map);
        return new ResponseEntity<>(mapList, HttpStatus.OK);
    }
}
