package com.feedback.customer.vuefeedbackapplication.controller.rest;

import com.feedback.customer.vuefeedbackapplication.customer.CustomerDTO;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class CustomerResource {

    @Autowired
    CustomerService customerService;

    @Autowired
    ResponseDetail responseDetail;

    @GetMapping("/customer")
    public ResponseEntity<ResponseDTO> getCategory() {
        ResponseDTO responseDTO;
        List<CustomerDTO> customerDTOList = customerService.findAll();
        if (customerDTOList != null) {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully retrieved Customer list", customerDTOList);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
        } else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.BAD_REQUEST, "Unable to retrieve Customer list", null);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.BAD_REQUEST);
        }

    }
}
