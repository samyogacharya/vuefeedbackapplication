package com.feedback.customer.vuefeedbackapplication.controller.rest;

import com.feedback.customer.vuefeedbackapplication.customer.CustomerDTO;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerError;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerService;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/open/api")
public class MainResource {

    @Autowired
    CustomerService customerService;

    @Autowired
    ResponseDetail responseDetail;

    @Autowired
    CustomerValidation customerValidation;

    @PostMapping("/customer/add")
    public ResponseEntity<ResponseDTO> addCustomer(@RequestBody CustomerDTO customerDTO) {
        ResponseDTO responseDTO;
        CustomerError customerError = customerValidation.validateCustomer(customerDTO,"add");
        if (customerError.getValid()) {
            customerService.save(customerDTO);
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully saved",
                    null);
        }else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.FAILURE, "Phone number already exists",
                    null);
        }
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }


}
