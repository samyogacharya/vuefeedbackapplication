package com.feedback.customer.vuefeedbackapplication.answer;

import com.feedback.customer.vuefeedbackapplication.configuration.SecurityUtility;
import com.feedback.customer.vuefeedbackapplication.customeranswer.CustomerAnswerDTO;
import com.feedback.customer.vuefeedbackapplication.customeranswer.CustomerAnswerService;
import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    AnswerRepository answerRepository;

    @Autowired
    AnswerConverter answerConverter;

    @Autowired
    SecurityUtility securityUtility;

    @Autowired
    CustomerAnswerService customerAnswerService;

    @Override
    public AnswerDTO save(AnswerDTO answerDTO) {
        Answer answer = answerConverter.toEntity(answerDTO);
        answer.setImages(answerDTO.getImages());
        answer = answerRepository.save(answer);
        return answerConverter.toDto(answer);
    }

    @Override
    public AnswerDTO findAnswerById(Long id) {
        return answerConverter.toDto(answerRepository.findAnswerById(id));
    }

    @Override
    public List<AnswerDTO> findAll() {
        return answerConverter.toDto(answerRepository.findByStatus(Status.ACTIVE));
    }

    @Override
    public AnswerDTO remarks(CustomerAnswerDTO customerAnswerDTO) {
        User user = securityUtility.getSecurity();
        Answer answer = answerRepository.findAnswerById(customerAnswerDTO.getAnswerId());
        if (user.getUserType().name().equalsIgnoreCase("SYSTEM_ADMIN")) {
            if (answer.getSystemAdminRemarks()==null||
                    answer.getSystemAdminRemarks().isEmpty()||
                    answer.getSystemAdminRemarks().trim().equals("")) {
                answer.setSystemAdminRemarks(customerAnswerDTO.getRemarks());
                answer.setSystemAdminRemarksDate(LocalDateTime.now());
            } else {
                answer.setSystemAdminRemarks2(customerAnswerDTO.getRemarks());
                answer.setSystemAdminRemarks2Date(LocalDateTime.now());
            }
        } else if (user.getUserType().name().equalsIgnoreCase("ADMIN")) {
            if (answer.getAdminRemarks()==null||
                    answer.getAdminRemarks().isEmpty()||
                    answer.getAdminRemarks().trim().equals("")) {
                answer.setAdminRemarks(customerAnswerDTO.getRemarks());
                answer.setAdminRemarksDate(LocalDateTime.now());
            } else {
                answer.setAdminRemarks2(customerAnswerDTO.getRemarks());
                answer.setAdminRemarks2Date(LocalDateTime.now());
            }
        }
        return answerConverter.toDto(answerRepository.save(answer));
    }

    @Override
    public AnswerDTO customerRemarks(CustomerAnswerDTO customerAnswerDTO){
        Answer answer = answerRepository.findAnswerById(customerAnswerDTO.getAnswerId());
        if (answer.getRemarks()==null||
                answer.getRemarks().isEmpty()||
                answer.getRemarks().trim().equals("")) {
            answer.setRemarks(customerAnswerDTO.getRemarks());
            answer.setRemarksDate(LocalDateTime.now());
        } else {
            answer.setRemarks2(customerAnswerDTO.getRemarks());
            answer.setRemarks2Date(LocalDateTime.now());
        }
        return answerConverter.toDto(answerRepository.save(answer));
    }

}
