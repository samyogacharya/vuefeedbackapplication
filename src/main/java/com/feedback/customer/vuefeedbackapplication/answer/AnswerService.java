package com.feedback.customer.vuefeedbackapplication.answer;

import com.feedback.customer.vuefeedbackapplication.customeranswer.CustomerAnswerDTO;

import java.util.List;

public interface AnswerService {

    AnswerDTO save(AnswerDTO answerDTO);

    AnswerDTO findAnswerById(Long id);

    List<AnswerDTO> findAll();

    AnswerDTO remarks(CustomerAnswerDTO customerAnswerDTO);

    AnswerDTO customerRemarks(CustomerAnswerDTO customerAnswerDTO);
}
