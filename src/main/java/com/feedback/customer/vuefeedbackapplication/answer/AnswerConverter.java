package com.feedback.customer.vuefeedbackapplication.answer;

import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.utility.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AnswerConverter implements EntityMapper<AnswerDTO, Answer> {

    @Autowired
    AnswerRepository answerRepository;

    @Override
    public Answer toEntity(AnswerDTO dto) {
        if (dto == null) {
            return null;
        }
        Answer entity;
        if (dto.getId() != null) {
            entity = answerRepository.findAnswerById(dto.getId());
        } else {
            entity = new Answer();
            toEntity(dto, entity);
            entity.setStatus(Status.ACTIVE);
            entity.setRemarks(dto.getRemarks());
            entity.setTitle(dto.getTitle());
            entity.setDescription(dto.getDescription());
            entity.setImages(dto.getImages());
        }
        return entity;
    }

    @Override
    public Answer toEntity(AnswerDTO dto, Answer entity) {
        if (entity == null || dto == null) {
            return null;
        }
        return entity;
    }

    @Override
    public AnswerDTO toDto(Answer entity) {
        if (entity == null) {
            return null;
        }
        AnswerDTO dto = new AnswerDTO();
        dto.setId(entity.getId());
        dto.setStatus(entity.getStatus());

        dto.setRemarks(entity.getRemarks());
        dto.setRemarks2(entity.getRemarks2());

        dto.setAdminRemarks(entity.getAdminRemarks());
        dto.setAdminRemarks2(entity.getAdminRemarks2());

        dto.setSystemAdminRemarks(entity.getSystemAdminRemarks());
        dto.setSystemAdminRemarks2(entity.getSystemAdminRemarks2());

        dto.setTitle(entity.getTitle());
        dto.setDescription(entity.getDescription());
        dto.setImages(entity.getImages());
        return dto;
    }

    @Override
    public List<Answer> toEntity(List<AnswerDTO> dtoList) {
        if (dtoList == null || dtoList.isEmpty()) {
            return null;
        }
        return dtoList.parallelStream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<AnswerDTO> toDto(List<Answer> entityList) {
        if (entityList == null) {
            return null;
        }
        return entityList.parallelStream().map(this::toDto).collect(Collectors.toList());
    }
}
