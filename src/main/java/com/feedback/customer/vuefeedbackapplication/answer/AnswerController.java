package com.feedback.customer.vuefeedbackapplication.answer;

import com.feedback.customer.vuefeedbackapplication.configuration.SecurityUtility;
import com.feedback.customer.vuefeedbackapplication.customeranswer.CustomerAnswerDTO;
import com.feedback.customer.vuefeedbackapplication.user.User;
import com.feedback.customer.vuefeedbackapplication.utility.ParameterConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "/auth")
public class AnswerController {

    @Autowired
    AnswerService answerService;

    @Autowired
    SecurityUtility securityUtility;

    @Autowired
    AnswerValidation answerValidation;

    @PostMapping("/adminremarks/")
    private String adminRemarks(CustomerAnswerDTO customerAnswerDTO, RedirectAttributes redirectAttributes) {
        AnswerDTO answerDTO = answerService.findAnswerById(customerAnswerDTO.getAnswerId());
        User user = securityUtility.getSecurity();

        AnswerError answerError =answerValidation.validateAnswer(customerAnswerDTO);
        if (answerError.getValid()) {
            answerService.remarks(customerAnswerDTO);
        }else {
            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_COMMENT_ERROR,answerError);
        }
        return "redirect:/auth/customerissue/detail/"+customerAnswerDTO.getId();
    }

}
