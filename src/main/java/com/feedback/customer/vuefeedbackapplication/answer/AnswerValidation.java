package com.feedback.customer.vuefeedbackapplication.answer;

import com.feedback.customer.vuefeedbackapplication.configuration.SecurityUtility;
import com.feedback.customer.vuefeedbackapplication.customeranswer.CustomerAnswerDTO;
import com.feedback.customer.vuefeedbackapplication.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnswerValidation {

    @Autowired
    SecurityUtility securityUtility;

    @Autowired
    AnswerService answerService;

    public AnswerError validateAnswer(CustomerAnswerDTO customerAnswerDTO){
        AnswerError answerError = new AnswerError();
        Boolean valid = true;
        User user = securityUtility.getSecurity();
        if (customerAnswerDTO.getRemarks()==null||
                customerAnswerDTO.getRemarks().trim().isEmpty())
        {
            valid=false;
            answerError.setComments("Invalid comment");
        }

        if (user.getUserType().name().equalsIgnoreCase("SYSTEM_ADMIN"))
        {
            AnswerDTO answerDTO1 = answerService.findAnswerById(customerAnswerDTO.getAnswerId());
            if (answerDTO1.getSystemAdminRemarks2()!=null){
                valid=false;
                answerError.setComments("Comment limit exceeds!");
            }
        }

        if (user.getUserType().name().equalsIgnoreCase("ADMIN"))
        {
            AnswerDTO answerDTO1 = answerService.findAnswerById(customerAnswerDTO.getAnswerId());
            if (answerDTO1.getAdminRemarks2()!=null){
                valid=false;
                answerError.setComments("Comment limit exceeds!");
            }
        }

        if (user.getUserType().name().equalsIgnoreCase("CUSTOMER"))
        {
            AnswerDTO answerDTO1 = answerService.findAnswerById(customerAnswerDTO.getAnswerId());
            if (answerDTO1.getRemarks2()!=null){
                valid=false;
                answerError.setComments("Comment limit exceeds!");
            }
        }
        answerError.setValid(valid);
        return answerError;
    }

}
