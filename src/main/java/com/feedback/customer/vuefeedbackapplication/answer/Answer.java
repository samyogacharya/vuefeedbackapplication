package com.feedback.customer.vuefeedbackapplication.answer;


import com.feedback.customer.vuefeedbackapplication.abstractclass.AbstractClass;
import com.feedback.customer.vuefeedbackapplication.user.Status;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "answer")
public class Answer extends AbstractClass<Long> {

    private String title;

    @Column(length = 10000)
    private String description;

    private Status status;

    private String remarks;

    private LocalDateTime remarksDate;

    private String remarks2;

    private LocalDateTime remarks2Date;

    private String adminRemarks;

    private LocalDateTime adminRemarksDate;

    private String adminRemarks2;

    private LocalDateTime adminRemarks2Date;

    private String systemAdminRemarks;

    private LocalDateTime systemAdminRemarksDate;

    private String systemAdminRemarks2;

    private LocalDateTime systemAdminRemarks2Date;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> images;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getRemarks2() {
        return remarks2;
    }

    public void setRemarks2(String remarks2) {
        this.remarks2 = remarks2;
    }

    public String getAdminRemarks() {
        return adminRemarks;
    }

    public void setAdminRemarks(String adminRemarks) {
        this.adminRemarks = adminRemarks;
    }

    public String getAdminRemarks2() {
        return adminRemarks2;
    }

    public void setAdminRemarks2(String adminRemarks2) {
        this.adminRemarks2 = adminRemarks2;
    }

    public String getSystemAdminRemarks() {
        return systemAdminRemarks;
    }

    public void setSystemAdminRemarks(String systemAdminRemarks) {
        this.systemAdminRemarks = systemAdminRemarks;
    }

    public String getSystemAdminRemarks2() {
        return systemAdminRemarks2;
    }

    public void setSystemAdminRemarks2(String systemAdminRemarks2) {
        this.systemAdminRemarks2 = systemAdminRemarks2;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getRemarksDate() {
        return remarksDate;
    }

    public void setRemarksDate(LocalDateTime remarksDate) {
        this.remarksDate = remarksDate;
    }

    public LocalDateTime getRemarks2Date() {
        return remarks2Date;
    }

    public void setRemarks2Date(LocalDateTime remarks2Date) {
        this.remarks2Date = remarks2Date;
    }

    public LocalDateTime getAdminRemarksDate() {
        return adminRemarksDate;
    }

    public void setAdminRemarksDate(LocalDateTime adminRemarksDate) {
        this.adminRemarksDate = adminRemarksDate;
    }

    public LocalDateTime getAdminRemarks2Date() {
        return adminRemarks2Date;
    }

    public void setAdminRemarks2Date(LocalDateTime adminRemarks2Date) {
        this.adminRemarks2Date = adminRemarks2Date;
    }

    public LocalDateTime getSystemAdminRemarksDate() {
        return systemAdminRemarksDate;
    }

    public void setSystemAdminRemarksDate(LocalDateTime systemAdminRemarksDate) {
        this.systemAdminRemarksDate = systemAdminRemarksDate;
    }

    public LocalDateTime getSystemAdminRemarks2Date() {
        return systemAdminRemarks2Date;
    }

    public void setSystemAdminRemarks2Date(LocalDateTime systemAdminRemarks2Date) {
        this.systemAdminRemarks2Date = systemAdminRemarks2Date;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
