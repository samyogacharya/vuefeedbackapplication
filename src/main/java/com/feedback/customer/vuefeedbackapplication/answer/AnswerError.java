package com.feedback.customer.vuefeedbackapplication.answer;

public class AnswerError {

    private String comments;

    private Boolean valid;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}
