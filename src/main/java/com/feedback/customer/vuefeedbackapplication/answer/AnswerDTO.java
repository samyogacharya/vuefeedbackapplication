package com.feedback.customer.vuefeedbackapplication.answer;

import com.feedback.customer.vuefeedbackapplication.user.Status;

import java.util.List;

public class AnswerDTO {

    private Long id;

    private String title;

    private String description;

    private String remarks;

    private Status status;

    private String remarks2;

    private String adminRemarks;

    private String adminRemarks2;

    private String systemAdminRemarks;

    private String systemAdminRemarks2;

    private List<String> images;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getRemarks2() {
        return remarks2;
    }

    public void setRemarks2(String remarks2) {
        this.remarks2 = remarks2;
    }

    public String getAdminRemarks() {
        return adminRemarks;
    }

    public void setAdminRemarks(String adminRemarks) {
        this.adminRemarks = adminRemarks;
    }

    public String getAdminRemarks2() {
        return adminRemarks2;
    }

    public void setAdminRemarks2(String adminRemarks2) {
        this.adminRemarks2 = adminRemarks2;
    }

    public String getSystemAdminRemarks() {
        return systemAdminRemarks;
    }

    public void setSystemAdminRemarks(String systemAdminRemarks) {
        this.systemAdminRemarks = systemAdminRemarks;
    }

    public String getSystemAdminRemarks2() {
        return systemAdminRemarks2;
    }

    public void setSystemAdminRemarks2(String systemAdminRemarks2) {
        this.systemAdminRemarks2 = systemAdminRemarks2;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
