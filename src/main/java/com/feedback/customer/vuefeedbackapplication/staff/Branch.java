package com.feedback.customer.vuefeedbackapplication.staff;

import com.feedback.customer.vuefeedbackapplication.abstractclass.AbstractClass;
import com.feedback.customer.vuefeedbackapplication.user.Shift;
import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.User;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "branch")
public class Branch extends AbstractClass<Long> {

    private String branchName;

//    private String branchCode;

    private Shift shift;

    private String address;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    private Status status;

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

   /* public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }*/

    public Shift getShift() {
        return shift;
    }

    public void setShift(Shift shift) {
        this.shift = shift;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
