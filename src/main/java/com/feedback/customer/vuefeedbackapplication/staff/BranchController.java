package com.feedback.customer.vuefeedbackapplication.staff;

import com.feedback.customer.vuefeedbackapplication.customer.CustomerService;
import com.feedback.customer.vuefeedbackapplication.customeranswer.CustomerAnswerService;
import com.feedback.customer.vuefeedbackapplication.user.UserDTO;
import com.feedback.customer.vuefeedbackapplication.user.UserError;
import com.feedback.customer.vuefeedbackapplication.user.UserService;
import com.feedback.customer.vuefeedbackapplication.user.UserValidation;
import com.feedback.customer.vuefeedbackapplication.utility.ParameterConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value="/auth")
public class BranchController {

    @Autowired
    BranchValidation branchValidation;

    @Autowired
    BranchService branchService;

    @Autowired
    UserValidation userValidation;

    @Autowired
    UserService userService;

    @Autowired
    CustomerService customerService;

    @Autowired
    CustomerAnswerService customerAnswerService;


    @Autowired
    BranchConverter branchConverter;

//    @GetMapping("/staff/add")
//    public String getBranchList(Model model) {
//        model.addAttribute(ParameterConstants.PARAM_SHIFT_LIST, java.util.Arrays.asList(Shift.values()));
//        model.addAttribute(ParameterConstants.PARAM_ACTION,"add");
//        return "staff/add";
//    }

//    @GetMapping("/staff/edit/{id}")
//    public String editBranch(@PathVariable("id")Long id, Model model) {
//        BranchDTO branchDTO=branchService.findBranchById(id);
//        UserDTO userDTO=userService.findUserById(branchDTO.getUserId());
//        userDTO.setPassword("");
//        model.addAttribute(ParameterConstants.PARAM_ACTION,"edit");
//        model.addAttribute(ParameterConstants.PARAM_BRANCH,branchDTO);
//        model.addAttribute(ParameterConstants.PARAM_USER,userDTO);
//        model.addAttribute(ParameterConstants.PARAM_SHIFT_LIST, branchDTO.getShift().toString());
//        return "staff/add";
//    }


    @GetMapping("/admin/staff/edit/{id}")
    public ResponseEntity<BranchUserDTO> editBranch(@PathVariable("id") Long id)
    {
        BranchDTO branchDTO=branchService.findBranchById(id);
        UserDTO userDTO=userService.findUserById(branchDTO.getUserId());
        userDTO.setPassword("");
        BranchUserDTO branchUserDTO=branchConverter.toBranchUserDTO(branchDTO, userDTO);
        return new ResponseEntity<>(branchUserDTO, HttpStatus.OK);
    }

    @PostMapping(value="/admin/staff/edit")
    public ResponseEntity<Map> editBranch(@RequestBody BranchUserDTO branchUserDTO)
    {
        Map<String, Object> map=new HashMap<>();
        BranchError branchError=branchValidation.validateBranch(branchUserDTO,"edit");
        if(branchError.getValid())
        {
            branchService.edit(branchUserDTO);
            map.put(ParameterConstants.PARAM_MESSAGE, "Success");
            map.put(ParameterConstants.PARAM_TEXT, "Edited Successfully.");
            return new ResponseEntity<>(map, HttpStatus.OK);
        }
        else{
            map.put(ParameterConstants.PARAM_ERROR,branchError);
            map.put(ParameterConstants.PARAM_BRANCH,branchUserDTO);
            map.put(ParameterConstants.PARAM_USER,branchUserDTO);
            map.put(ParameterConstants.PARAM_MESSAGE,"Staff not edited!");
            return new ResponseEntity<>(map, HttpStatus.OK);
        }
    }

//    @PostMapping("/staff/add")
//    public String saveBranch(RedirectAttributes redirectAttributes, BranchDTO branchDTO, UserDTO userDto){
//        BranchError branchError=branchValidation.validateBranch(branchDTO,"add");
//        UserError userError=userValidation.validateUser(userDto,"add");
//        if (branchError.getValid()&&userError.getValid()){
//            branchService.save(userDto,branchDTO);
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_MESSAGE, "Success");
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_TEXT, "Added Sucessfully.");
//            return "redirect:/auth/admin/staff/list";
//        }else {
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_ERROR,branchError);
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_USER_ERROR,userError);
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_BRANCH,branchDTO);
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_USER,userDto);
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_VALID,"show");
//            redirectAttributes.addFlashAttribute(ParameterConstants.PARAM_MESSAGE,"Staff not added!");
//            return "redirect:/auth/admin/staff/add";
//        }
//    }

    @PostMapping(value="/admin/staff/add")
    public ResponseEntity<Map> saveBranch(@RequestBody BranchUserDTO branchUserDTO)
    {
        BranchError branchError=branchValidation.validateBranch(branchUserDTO, "add");
        UserError userError=userValidation.validateUser(branchUserDTO,"add");
        if (branchError.getValid()&&userError.getValid()) {
            branchService.save(branchUserDTO);
            Map<String, Object> map=new HashMap<>();
            map.put(ParameterConstants.PARAM_MESSAGE, "Success");
            map.put(ParameterConstants.PARAM_TEXT, "Added Successfully");
            return new ResponseEntity(map, HttpStatus.OK);
        }
        else {
            Map<String, Object> map=new HashMap<>();
            map.put(ParameterConstants.PARAM_ERROR,branchError);
            map.put(ParameterConstants.PARAM_USER_ERROR,userError);
            map.put(ParameterConstants.PARAM_BRANCH,branchUserDTO);
            map.put(ParameterConstants.PARAM_USER,branchUserDTO);
            map.put(ParameterConstants.PARAM_VALID,"show");
            map.put(ParameterConstants.PARAM_MESSAGE,"Staff not added!");
            return new ResponseEntity(map, HttpStatus.OK);
        }
    }


//    @PostMapping("/staff/edit")
//    public String editBranch(RedirectAttributes redirectAttribute, BranchDTO branchDTO,
//                             UserDTO userDTO, @ModelAttribute("userId")Long uid){
//        BranchError branchError=branchValidation.validateBranch(branchDTO,"edit");
//        branchDTO.setUserId(uid);
//        userDTO.setId(uid);
//        UserError userError=userValidation.validateUser(userDTO,"edit");
//        if (branchError.getValid()&&userError.getValid()) {
//            branchService.edit(userDTO,branchDTO);
//            redirectAttribute.addFlashAttribute(ParameterConstants.PARAM_MESSAGE, "Success");
//            redirectAttribute.addFlashAttribute(ParameterConstants.PARAM_TEXT, "Edited Sucessfully.");
//            return "redirect:/auth/admin/staff/list";
//        }else {
//            redirectAttribute.addFlashAttribute(ParameterConstants.PARAM_ERROR,branchError);
//            redirectAttribute.addFlashAttribute(ParameterConstants.PARAM_USER_ERROR,userError);
//            redirectAttribute.addFlashAttribute(ParameterConstants.PARAM_BRANCH,branchDTO);
//            redirectAttribute.addFlashAttribute(ParameterConstants.PARAM_USER,userDTO);
//            redirectAttribute.addFlashAttribute(ParameterConstants.PARAM_VALID,"show");
//            redirectAttribute.addFlashAttribute(ParameterConstants.PARAM_MESSAGE,"Staff not edited!");
//            return "redirect:/auth/admin/staff/edit/"+branchDTO.getId();
//        }
//    }




//    @GetMapping("/staff/list")
//    public String saveBranchList(Model model) {
//        model.addAttribute(ParameterConstants.PARAM_BRANCH,branchService.findAll());
//        return "staff/list";
//    }
//
       @GetMapping("/admin/staff/list")
      public ResponseEntity<List<BranchDTO>> getBranchList()
      {
          List<BranchDTO> branchDTOList=branchService.findAll();
          return new ResponseEntity<>(branchDTOList, HttpStatus.OK);
      }
}
