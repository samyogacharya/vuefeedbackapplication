package com.feedback.customer.vuefeedbackapplication.staff;

import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.UserDTO;
import com.feedback.customer.vuefeedbackapplication.user.UserRepository;
import com.feedback.customer.vuefeedbackapplication.utility.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BranchConverter implements EntityMapper<BranchDTO, Branch> {

    @Autowired
    BranchRepository branchRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public Branch toEntity(BranchDTO dto) {
        if (dto == null) {
            return null;
        }
        Branch entity;
        if (dto.getId() != null) {
            entity = branchRepository.findBranchById(dto.getId());
        } else {
            entity = new Branch();
            toEntity(dto, entity);
            entity.setStatus(Status.ACTIVE);
        }
        return entity;
    }

    public BranchUserDTO toBranchUserDTO(BranchDTO branchDTO, UserDTO userDTO)
    {
        BranchUserDTO branchUserDTO=new BranchUserDTO();
        branchUserDTO.setId(branchDTO.getId());
        branchUserDTO.setUserId(userDTO.getId());
        branchUserDTO.setBranchName(branchDTO.getBranchName());
        branchUserDTO.setUserType(userDTO.getUserType());
        branchUserDTO.setAddress(branchDTO.getAddress());
        branchUserDTO.setUsername(userDTO.getUsername());
        branchUserDTO.setPassword(userDTO.getPassword());
        branchUserDTO.setShift(branchDTO.getShift());
        branchUserDTO.setStatus(branchDTO.getStatus());
        branchUserDTO.setEmail(userDTO.getEmail());
        return branchUserDTO;
    }


    public Branch convertToEntity(BranchDTO dto) {
        if (dto == null) {
            return null;
        }
        Branch entity = new Branch();
            toEntity(dto, entity);
            entity.setStatus(Status.ACTIVE);
        return entity;
    }

    @Override
    public Branch toEntity(BranchDTO dto, Branch entity) {
        if (entity == null || dto == null) {
            return null;
        }
        entity.setBranchName(dto.getBranchName());
        entity.setShift(dto.getShift());
//        entity.setBranchCode(dto.getBranchCode());
        entity.setAddress(dto.getAddress());
        entity.setUser(userRepository.findUserById(dto.getUserId()));
        return entity;
    }

    @Override
    public BranchDTO toDto(Branch entity) {
        if (entity == null) {
            return null;
        }
        BranchDTO dto = new BranchDTO();
        dto.setId(entity.getId());
        dto.setBranchName(entity.getBranchName());
        dto.setShift(entity.getShift());
//        dto.setBranchCode(entity.getBranchCode());
        dto.setAddress(entity.getAddress());
        dto.setStatus(entity.getStatus());
        dto.setUserId(entity.getUser().getId());
        dto.setUserLogin(entity.getUser().getUsername());
        return dto;
    }






    @Override
    public List<Branch> toEntity(List<BranchDTO> dtoList) {
        if (dtoList == null || dtoList.isEmpty()) {
            return null;
        }
        return dtoList.parallelStream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<BranchDTO> toDto(List<Branch> entityList) {
        if (entityList == null) {
            return null;
        }
        return entityList.parallelStream().map(this::toDto).collect(Collectors.toList());
    }
}
