package com.feedback.customer.vuefeedbackapplication.staff;


import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.User;
import com.feedback.customer.vuefeedbackapplication.user.UserDTO;

import java.util.List;

public interface BranchService {

    BranchDTO save(UserDTO userDto, BranchDTO branchDTO);


    void save(BranchUserDTO branchUserDTO);

    BranchDTO findByBranchNameAndStatus(String name, Status status);

    Integer countBranchByStatus(Status status);

    List<BranchDTO> findAll();

    BranchDTO findByUserAndStatus(User user);

    BranchDTO edit(UserDTO userDTO, BranchDTO branchDTO);
    void  edit(BranchUserDTO branchUserDTO);
    BranchDTO findBranchById(Long id);
    List<BranchDTO> findListOfBranchById(Long id);
    BranchDTO findBranchesByUserId(Long userId);

}
