package com.feedback.customer.vuefeedbackapplication.staff;

import com.feedback.customer.vuefeedbackapplication.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BranchServiceImpl implements BranchService {

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private BranchConverter branchConverter;

    @Autowired
    private UserConverter userConverter;

    @Autowired
    UserRepository userRepository;


    @Autowired
    UserService userService;

    @Override
    public BranchDTO save(UserDTO userDto, BranchDTO branchDTO) {
        userDto.setUserType(UserType.ADMIN);
        userDto.setStatus(Status.ACTIVE);
        User user = userConverter.toEntity(userService.save(userDto));
        Branch branch = branchConverter.toEntity(branchDTO);
        branch.setUser(user);
        branch = branchRepository.save(branch);
        return branchConverter.toDto(branch);
    }

    @Override
    public void save(BranchUserDTO branchUserDTO) {
         UserDTO userDTO=new UserDTO();
         userDTO.setEmail(branchUserDTO.getEmail());
         userDTO.setUsername(branchUserDTO.getUsername());
         userDTO.setPassword(branchUserDTO.getPassword());
         userDTO.setUserType(UserType.ADMIN);
         userDTO.setStatus(Status.ACTIVE);
         User user=userConverter.toEntity(userService.save(userDTO));
         BranchDTO branchDTO=new BranchDTO();
         branchDTO.setBranchName(branchUserDTO.getBranchName());
         branchDTO.setAddress(branchUserDTO.getAddress());
         branchDTO.setShift(branchUserDTO.getShift());
        Branch branch = branchConverter.toEntity(branchDTO);
        branch.setUser(user);
        branch=branchRepository.save(branch);
    }


    @Override
    public BranchDTO findByBranchNameAndStatus(String name, Status status) {
        return branchConverter.toDto(branchRepository.findByBranchNameAndStatus(name, status));
    }

    @Override
    public Integer countBranchByStatus(Status status) {
        return branchRepository.countBranchByStatus(Status.ACTIVE);
    }

    @Override
    public List<BranchDTO> findAll() {
        return branchConverter.toDto(branchRepository.findByStatus(Status.ACTIVE));
    }

    @Override
    public BranchDTO findByUserAndStatus(User user) {
        return branchConverter.toDto(branchRepository.findByUserAndStatus(user, Status.ACTIVE));
    }

    @Override
    public BranchDTO edit(UserDTO userDTO, BranchDTO branchDTO) {
//        UserDTO userDTO1=userService.save(userDTO);
        Branch branch=branchConverter.toEntity(branchDTO);
        branch.setAddress(branchDTO.getAddress());
        branch.setShift(branchDTO.getShift());
//        branch.setBranchCode(branchDTO.getBranchCode());
        branch.setBranchName(branchDTO.getBranchName());
//        Branch branch=branchConverter.convertToEntity(branchDTO);
        User user=(branch.getUser());
        user.setEmail(userDTO.getEmail());
        user.setUsername(userDTO.getUsername());
//        user.setPassword(hashPassword.hashPassword(userDTO.getPassword()));
        userRepository.save(user);
        return branchConverter.toDto(branchRepository.save(branch));
    }

    @Override
    public void edit(BranchUserDTO branchUserDTO) {
        BranchDTO branchDTO=new BranchDTO();
        branchDTO.setId(branchUserDTO.getId());
        branchDTO.setUserId(branchUserDTO.getUserId());
        branchDTO.setBranchName(branchUserDTO.getBranchName());
        branchDTO.setShift(branchUserDTO.getShift());
        branchDTO.setStatus(branchUserDTO.getStatus());
        branchDTO.setAddress(branchUserDTO.getAddress());
        Branch branch=branchConverter.convertToEntity(branchDTO);
        branchRepository.save(branch);
        User user=(branch.getUser());
        user.setEmail(branchUserDTO.getEmail());
        user.setUsername(branchUserDTO.getUsername());
        userRepository.save(user);
    }

    @Override
    public BranchDTO findBranchById(Long id) {
        BranchDTO branchDTO=branchConverter.toDto(branchRepository.findBranchById(id));
        return branchDTO;
    }

    @Override
    public List<BranchDTO> findListOfBranchById(Long id) {
          List<Branch> branchList=branchRepository.findAllById(id);
        return branchConverter.toDto(branchList);
    }

    @Override
    public BranchDTO findBranchesByUserId(Long userId) {
        Branch branch=branchRepository.findBranchesByUserId(userId);
        return branchConverter.toDto(branch);
    }

}
