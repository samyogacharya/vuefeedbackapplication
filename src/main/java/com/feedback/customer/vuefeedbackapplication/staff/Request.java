package com.feedback.customer.vuefeedbackapplication.staff;

import java.util.Map;

public class Request {

    private Map<String, Object> branchDTO;

    public Map<String, Object> getBranchDTO() {
        return branchDTO;
    }

    public void setBranchDTO(Map<String, Object> branchDTO) {
        this.branchDTO = branchDTO;
    }
}
