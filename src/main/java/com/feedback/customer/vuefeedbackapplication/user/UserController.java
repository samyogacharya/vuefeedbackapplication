package com.feedback.customer.vuefeedbackapplication.user;


import com.feedback.customer.vuefeedbackapplication.configuration.SecurityUtility;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerService;
import com.feedback.customer.vuefeedbackapplication.parkingdetails.ParkingDetailsService;
import com.feedback.customer.vuefeedbackapplication.staff.BranchService;
import com.feedback.customer.vuefeedbackapplication.utility.ParameterConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value="/auth")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    BranchService branchService;

    @Autowired
    CustomerService customerService;

    @Autowired
    SecurityUtility securityUtility;

    @Autowired
    ChangePasswordValidation changePasswordValidation;

    @Autowired
    ParkingDetailsService parkingDetailsService;


    @PostMapping("/changePassword")
    public ResponseEntity<Map> setChangePassword(@RequestBody ChangePasswordDTO changePasswordDTO)
    {
        Map<String, Object> map=new HashMap<>();
        ChangePasswordError changePasswordError = changePasswordValidation.validateChangePassword(changePasswordDTO);
        if(changePasswordError.getValid())
        {
            userService.changePassword(changePasswordDTO);
            map.put(ParameterConstants.PARAM_MESSAGE, "Success");
            map.put("/logout-success", "/logout-success");
            return new ResponseEntity<>(map, HttpStatus.OK);
        }
        else {
            map.put(ParameterConstants.PARAM_MESSAGE, "Warning");
            map.put(ParameterConstants.PARAM_ERROR, changePasswordError);
            map.put(ParameterConstants.PARAM_CHANGE_PASSWORD, changePasswordDTO);
            return new ResponseEntity<>(map, HttpStatus.OK);
        }
    }



}