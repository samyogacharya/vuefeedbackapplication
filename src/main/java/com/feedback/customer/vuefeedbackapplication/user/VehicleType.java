package com.feedback.customer.vuefeedbackapplication.user;

public enum VehicleType {
    TWO_WHEELER, FOUR_WHEELER, SIX_WHEELER;
}
