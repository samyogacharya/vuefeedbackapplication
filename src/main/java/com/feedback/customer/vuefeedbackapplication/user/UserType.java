package com.feedback.customer.vuefeedbackapplication.user;

public enum UserType {

    SYSTEM_ADMIN, ADMIN, CUSTOMER;

}
