package com.feedback.customer.vuefeedbackapplication.user;

public enum FilterEnum {
    DAILY, WEEKLY, MONTHLY, ALL
}
