package com.feedback.customer.vuefeedbackapplication.user;

public enum AnswerType {

    EMOJI_RATINGS, STAR_RATINGS;
}
