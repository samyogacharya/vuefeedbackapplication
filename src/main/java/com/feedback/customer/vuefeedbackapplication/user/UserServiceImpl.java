package com.feedback.customer.vuefeedbackapplication.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;


    @Autowired
    UserConverter userConverter;


    @Override
    public UserDTO save(UserDTO userDto) {
        User user = userConverter.convertToEntity(userDto);
        user = userRepository.save(user);
        return userConverter.toDto(user);
    }

    @Override
    public User findByUserName(String username) {
        return userRepository.findByUsernameAndStatus(username,Status.ACTIVE);
    }

    @Override
    public UserDTO findUserById(Long id) {
        return userConverter.toDto(userRepository.findUserById(id));
    }

    @Override
    public UserDTO changePassword(ChangePasswordDTO changePasswordDTO) {
        return null;
//        return userConverter.toDto(userRepository.save(user));
    }

    @Override
    public List<UserDTO> findAll() {
        return userConverter.toDto(userRepository.findByStatus(Status.ACTIVE));
    }

}
