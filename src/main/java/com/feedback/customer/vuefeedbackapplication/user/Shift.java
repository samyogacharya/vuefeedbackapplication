package com.feedback.customer.vuefeedbackapplication.user;

public enum Shift {
    MORNING, DAY, EVENING;
}
