package com.feedback.customer.vuefeedbackapplication.parkingdetails;


import com.feedback.customer.vuefeedbackapplication.staff.BranchDTO;
import com.feedback.customer.vuefeedbackapplication.staff.BranchService;
import com.feedback.customer.vuefeedbackapplication.user.FilterEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value="/auth")
public class ParkingDetailsController {


    @Autowired
    ParkingDetailsService parkingDetailsService;

    @Autowired
    BranchService branchService;

    @GetMapping(value = "/admin/user/List/{filterEnum}")
    public ResponseEntity<List<ParkingDetailsDTO>> listResponseEntity(@PathVariable FilterEnum filterEnum) throws ParseException {
        LocalDate currentDate = LocalDate.from((LocalDate.now().plusDays(1)));     //Today
        LocalDate asYesterday = currentDate.minusDays(1);
        LocalDate asWeekAgo = currentDate.minusDays(7);
        LocalDate asMonthAgo = currentDate.minusDays(31);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date today = dateFormat.parse(String.valueOf(currentDate));
        Date yesterday = dateFormat.parse(String.valueOf(asYesterday));
        Date week = dateFormat.parse(String.valueOf(asWeekAgo));
        Date month = dateFormat.parse(String.valueOf(asMonthAgo));
        if (filterEnum != null) {
            if (filterEnum.equals(FilterEnum.MONTHLY)) {
                List<ParkingDetailsDTO> parkingDetailsDTO = parkingDetailsService.findAllByStatusAndCreatedBetween(today, month);
                return new ResponseEntity<>(parkingDetailsDTO, HttpStatus.OK);
            } else if (filterEnum.equals(FilterEnum.WEEKLY)) {
                List<ParkingDetailsDTO> parkingDetailsDTO = parkingDetailsService.findAllByStatusAndCreatedBetween(today, week);
                return new ResponseEntity<>(parkingDetailsDTO, HttpStatus.OK);
            } else if (filterEnum.equals(FilterEnum.DAILY)) {
                List<ParkingDetailsDTO> parkingDetailsDTO = parkingDetailsService.findAllByStatusAndCreatedBetween(today, yesterday);
                return new ResponseEntity<>(parkingDetailsDTO, HttpStatus.OK);
            } else if (filterEnum.equals(FilterEnum.ALL)) {
                List<ParkingDetailsDTO> parkingDetailsDTO = parkingDetailsService.findAll();
                return new ResponseEntity<>(parkingDetailsDTO, HttpStatus.OK);
            } else {
                List<ParkingDetailsDTO> parkingDetailsDTO = parkingDetailsService.findAll();
                return new ResponseEntity<>(parkingDetailsDTO, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @GetMapping(value="/admin/invoices/branch/{userId}")
    public ResponseEntity<BranchDTO> findIndividualBranch(@PathVariable long userId)
    {
        BranchDTO branchDTO=branchService.findBranchesByUserId(userId);
        return new ResponseEntity<>(branchDTO, HttpStatus.OK);
    }


    @GetMapping(value="/admin/invoices/getIndividualData/{userId}")
    public ResponseEntity<Map> getIndividualData(@PathVariable long userId) throws ParseException {
        Map<String, Object> map=new HashMap<>();
        map.put("IndividualData", parkingDetailsService.getStaffsIndividualData(userId));
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping(value="/admin/invoices/listById/{userId}/{filterEnum}")
    public ResponseEntity<List<ParkingDetailsDTO>> getStaffReportByDate(@PathVariable long userId,@PathVariable FilterEnum filterEnum) throws ParseException {
        LocalDate currentDate = LocalDate.from((LocalDate.now().plusDays(1)));     //Today
        LocalDate asYesterday = currentDate.minusDays(1);
        LocalDate asWeekAgo = currentDate.minusDays(7);
        LocalDate asMonthAgo = currentDate.minusDays(31);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date today = formatter.parse(String.valueOf(currentDate));
        Date yesterday = formatter.parse(String.valueOf(asYesterday));
        Date week = formatter.parse(String.valueOf(asWeekAgo));
        Date month = formatter.parse(String.valueOf(asMonthAgo));
        BranchDTO branchDTO = branchService.findBranchesByUserId(userId);
        List<ParkingDetailsDTO> parkingDetailsDTOList = parkingDetailsService.findByBranch(branchDTO.getId());
        if (filterEnum != null) {
            if (filterEnum.equals(FilterEnum.MONTHLY)) {
                List<ParkingDetailsDTO> parkingDetailsDTOS = parkingDetailsService.findAllByBranchAndCreatedBetween(branchDTO.getId(), today, month);
                return new ResponseEntity<>(parkingDetailsDTOS, HttpStatus.OK);
            } else if (filterEnum.equals(FilterEnum.WEEKLY)) {
                List<ParkingDetailsDTO> parkingDetailsDTOS = parkingDetailsService.findAllByBranchAndCreatedBetween(branchDTO.getId(), today, week);
                return new ResponseEntity<>(parkingDetailsDTOS, HttpStatus.OK);
            } else if (filterEnum.equals(FilterEnum.DAILY)) {
                List<ParkingDetailsDTO> parkingDetailsDTOS = parkingDetailsService.findAllByBranchAndCreatedBetween(branchDTO.getId(), today, yesterday);
                return new ResponseEntity<>(parkingDetailsDTOS, HttpStatus.OK);
            } else if (filterEnum.equals(FilterEnum.ALL)) {
                List<ParkingDetailsDTO> parkingDetailsDTOS = parkingDetailsService.findByBranch(branchDTO.getId());
                return new ResponseEntity<>(parkingDetailsDTOS, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(parkingDetailsDTOList, HttpStatus.OK);
    }
}
