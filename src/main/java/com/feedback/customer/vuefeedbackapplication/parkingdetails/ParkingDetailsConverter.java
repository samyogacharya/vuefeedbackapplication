package com.feedback.customer.vuefeedbackapplication.parkingdetails;

import com.feedback.customer.vuefeedbackapplication.category.CategoryRepository;
import com.feedback.customer.vuefeedbackapplication.staff.BranchRepository;
import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.utility.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ParkingDetailsConverter implements EntityMapper<ParkingDetailsDTO, ParkingDetails> {

    @Autowired
    BranchRepository branchRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public ParkingDetails toEntity(ParkingDetailsDTO dto) {
        if (dto == null) {
            return null;
        }
        ParkingDetails entity = new ParkingDetails();
        entity = toEntity(dto,entity);
        entity.setStatus(Status.ACTIVE);
        return entity;
    }

    @Override
    public ParkingDetails toEntity(ParkingDetailsDTO dto, ParkingDetails entity) {
        if (entity == null || dto == null) {
            return null;
        }

        entity.setCheckIn(LocalDateTime.parse(dto.getCheckIn()));
        if (dto.getCheckOut()!=null) {
            entity.setCheckOut(LocalDateTime.parse(dto.getCheckOut()));
        }
        entity.setDuration(dto.getDuration());
        entity.setBranch(branchRepository.findBranchById(dto.getStaffId()));
        entity.setCategory(categoryRepository.findCategoryById(dto.getCategoryId()));
        entity.setPrice(dto.getPrice());
        entity.setVehicleNumber(dto.getVehicleNumber());
        entity.setStatus(dto.getStatus());
        return entity;
    }

    @Override
    public ParkingDetailsDTO toDto(ParkingDetails entity) {
        if (entity == null){
            return null;
        }
        ParkingDetailsDTO dto = new ParkingDetailsDTO();
        dto.setId(entity.getId());
        dto.setCategoryId(entity.getCategory().getId());
        dto.setCheckIn(String.valueOf(entity.getCheckIn()));
        dto.setRate(entity.getCategory().getRate());
        dto.setCategoryName(entity.getCategory().getCategoryName());
        dto.setCheckOut(String.valueOf(entity.getCheckOut()));
        dto.setDuration(entity.getDuration());
        dto.setPrice(entity.getPrice());
        dto.setStaffId(entity.getBranch().getId());
        dto.setStaffName(entity.getBranch().getBranchName());
        dto.setRate(dto.getRate());
        dto.setStatus(entity.getStatus());
        dto.setVehicleNumber(entity.getVehicleNumber());
        return dto;
    }

    @Override
    public List<ParkingDetails> toEntity(List<ParkingDetailsDTO> dtoList) {
        if (dtoList == null || dtoList.isEmpty()) {
            return null;
        }
        return dtoList.parallelStream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<ParkingDetailsDTO> toDto(List<ParkingDetails> entityList) {
        if (entityList == null) {
            return null;
        }
        return entityList.parallelStream().map(this::toDto).collect(Collectors.toList());
    }
}
