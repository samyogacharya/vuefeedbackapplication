package com.feedback.customer.vuefeedbackapplication.parkingdetails;

import com.feedback.customer.vuefeedbackapplication.user.VehicleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ParkingRepository extends JpaRepository<ParkingDetails, Long> {

    Optional<ParkingDetails> findById(Long id);

    List<ParkingDetails> findByBranchId(Long id);

    List<ParkingDetails> findByCategory_Id(Long id);

    List<ParkingDetails> findByBranch_IdAndCategory_Id(Long bid, Long cid);

    @Query("SELECT sum(p.price) from ParkingDetails p")
    Long findTotalPrice();

    @Query("SELECT COUNT(p.id) FROM ParkingDetails p")
    Integer countTotalTransaction();

    @Query("SELECT sum(p.price) FROM ParkingDetails p WHERE p.branch.id=?1")
    Double findTotalPriceByBranchId(Long branchId);

    @Query("SELECT count(p.id) FROM ParkingDetails p WHERE p.branch.id=?1")
    Integer countTotalTransactionByBranchId(Long branchId);



    @Query("select p from ParkingDetails p where p.created between ?2 and ?1")
    List<ParkingDetails> findAllByCreatedBetween(Date endDate, Date startDate);

    @Query("select p from ParkingDetails  p where p.branch.id=?1 and p.created between ?3 and ?2")
    List<ParkingDetails> findAllByBranch_IdAndCreatedBetween(Long id, Date endDate, Date startDate);

    Long countByCategory_VehicleType(VehicleType vehicleType);

    @Query("SELECT count(p.id) FROM ParkingDetails p WHERE p.branch.id=?1 and p.created between ?3 and ?2")
    Integer countTotalTransactionByBranchIdAndCreatedBetween(Long branchId, Date endDate, Date startDate);

    Integer countByCategoryVehicleTypeAndBranchId(VehicleType vehicleType, Long branchId);

}
