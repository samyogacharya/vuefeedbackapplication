package com.feedback.customer.vuefeedbackapplication.parkingdetails;

import com.feedback.customer.vuefeedbackapplication.user.VehicleType;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ParkingDetailsService {

    ParkingDetailsDTO save(ParkingDetailsDTO parkingDetailsDTO);

    ParkingDetailsDTO findOne(Long id);

    List<ParkingDetailsDTO> findAll();

    List<ParkingDetailsDTO> findByBranch(Long id);

    Integer countTotalTransaction();

    Double findTotalPriceByBranchId(Long branchId);

    Integer countTotalTransactionByBranchId(Long branchId);
    Integer countTotalTransactionByBranchIdAndCreatedBetween(Long branchId, Date endDate, Date startDate);

    List<ParkingDetailsDTO> findByCategory(Long id);

    List<ParkingDetailsDTO> findByBranchAndCategory(Long bid, Long cid);

    Long findTotalPrice();
    List<ParkingDetailsDTO> findAllByStatusAndCreatedBetween(Date endDate, Date startDate);
    List<ParkingDetailsDTO> findAllByBranchAndCreatedBetween(Long id, Date endDate, Date startDate);
    Long countByCategory_VehicleType(VehicleType vehicleType);
    Integer countByCategoryVehicleTypeAndBranchId(VehicleType vehicleType, Long branchId);
    Map getStaffsIndividualData(Long id) throws ParseException;

}
