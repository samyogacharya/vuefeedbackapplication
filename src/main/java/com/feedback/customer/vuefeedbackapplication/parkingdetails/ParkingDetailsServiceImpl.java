package com.feedback.customer.vuefeedbackapplication.parkingdetails;

import com.feedback.customer.vuefeedbackapplication.staff.BranchDTO;
import com.feedback.customer.vuefeedbackapplication.staff.BranchService;
import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.VehicleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class ParkingDetailsServiceImpl implements ParkingDetailsService {

    @Autowired
    ParkingDetailsConverter parkingDetailsConverter;

    @Autowired
    ParkingRepository parkingRepository;

    @Autowired
    BranchService branchService;



    @Override
    public ParkingDetailsDTO save(ParkingDetailsDTO parkingDetailsDTO) {
        Optional<ParkingDetails> parkingDetails = Optional.of(new ParkingDetails());
        if (parkingDetailsDTO.getId() != null) {
            parkingDetails = parkingRepository.findById(parkingDetailsDTO.getId());
            parkingDetails.get().setStatus(Status.ACTIVE);
            parkingDetails.get().setCheckOut(LocalDateTime.parse(parkingDetailsDTO.getCheckOut()));
            parkingDetails.get().setDuration(parkingDetailsDTO.getDuration());
            parkingDetails.get().setPrice(parkingDetailsDTO.getPrice());
            parkingRepository.save(parkingDetails.get());
        } else {
            ParkingDetails parkingDetails1 = parkingRepository.save(parkingDetailsConverter.toEntity(parkingDetailsDTO));
            return parkingDetailsConverter.toDto(parkingDetails1);
        }
        return parkingDetailsConverter.toDto(parkingDetails.get());
    }

    @Override
    public ParkingDetailsDTO findOne(Long id) {
        return parkingDetailsConverter.toDto(parkingRepository.findById(id).get());
    }

    @Override
    public List<ParkingDetailsDTO> findAll() {
        return parkingDetailsConverter.toDto(parkingRepository.findAll());
    }

    @Override
    public List<ParkingDetailsDTO> findByBranch(Long id) {
        return parkingDetailsConverter.toDto(parkingRepository.findByBranchId(id));
    }

    @Override
    public Integer countTotalTransaction() {
        return parkingRepository.countTotalTransaction();
    }

    @Override
    public Double findTotalPriceByBranchId(Long branchId) {
        return parkingRepository.findTotalPriceByBranchId(branchId);
    }

    @Override
    public Integer countTotalTransactionByBranchId(Long branchId) {
        return parkingRepository.countTotalTransactionByBranchId(branchId);
    }

    @Override
    public Integer countTotalTransactionByBranchIdAndCreatedBetween(Long branchId, Date endDate, Date startDate) {
        return parkingRepository.countTotalTransactionByBranchIdAndCreatedBetween(branchId, endDate, startDate);
    }

    @Override
    public List<ParkingDetailsDTO> findByCategory(Long id) {
        return parkingDetailsConverter.toDto(parkingRepository.findByCategory_Id(id));
    }

    @Override
    public List<ParkingDetailsDTO> findByBranchAndCategory(Long bid, Long cid) {
        return parkingDetailsConverter.toDto(parkingRepository.findByBranch_IdAndCategory_Id(bid, cid));
    }

    @Override
    public Long findTotalPrice() {
        return parkingRepository.findTotalPrice();
    }

    @Override
    public List<ParkingDetailsDTO> findAllByStatusAndCreatedBetween(Date endDate, Date startDate) {
        /*DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date today = dateFormat.parse(String.valueOf("2019-09-09 10:42:27"));
        Date week = dateFormat.parse(String.valueOf("2019-09-12 17:02:43"));

        System.out.println(today+"parse service impl");*/
        List<ParkingDetails> parkingDetails = parkingRepository.findAllByCreatedBetween(endDate, startDate);
        return parkingDetailsConverter.toDto(parkingDetails);
    }

    @Override
    public List<ParkingDetailsDTO> findAllByBranchAndCreatedBetween(Long id, Date endDate, Date startDate) {
        List<ParkingDetails> parkingDetails = parkingRepository.findAllByBranch_IdAndCreatedBetween(id, endDate, startDate);
        return parkingDetailsConverter.toDto(parkingDetails);
    }

    @Override
    public Long countByCategory_VehicleType(VehicleType vehicleType) {
        return parkingRepository.countByCategory_VehicleType(vehicleType);
    }

    @Override
    public Integer countByCategoryVehicleTypeAndBranchId(VehicleType vehicleType, Long branchId) {
        return parkingRepository.countByCategoryVehicleTypeAndBranchId(vehicleType, branchId);
    }

    @Override
    public Map getStaffsIndividualData(Long id) throws ParseException {
        BranchDTO branchDTO=branchService.findBranchesByUserId(id);
        Map<String, Object> map=new HashMap<>();
        LocalDate currentDate = LocalDate.from((LocalDate.now().plusDays(1)));     //Today
        LocalDate asYesterday = currentDate.minusDays(1);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date today = formatter.parse(String.valueOf(currentDate));
        Date yesterday = formatter.parse(String.valueOf(asYesterday));
        Double totalIncome=findTotalPriceByBranchId(branchDTO.getId());
        Integer totalTransaction=countTotalTransactionByBranchId(branchDTO.getId());
        Integer todayTotalTransaction=countTotalTransactionByBranchIdAndCreatedBetween(branchDTO.getId(), today, yesterday);
        map.put("totalIncome", totalIncome);
        map.put("totalTransaction", totalTransaction);
        map.put("todayTotalTransaction", todayTotalTransaction);
        map.put("two", countByCategoryVehicleTypeAndBranchId(VehicleType.TWO_WHEELER, branchDTO.getId()));
        map.put("four", countByCategoryVehicleTypeAndBranchId(VehicleType.FOUR_WHEELER, branchDTO.getId()));
        map.put("six", countByCategoryVehicleTypeAndBranchId(VehicleType.SIX_WHEELER, branchDTO.getId()));
        return map;
    }


}
