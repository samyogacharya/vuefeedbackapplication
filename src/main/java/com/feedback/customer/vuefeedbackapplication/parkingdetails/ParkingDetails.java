package com.feedback.customer.vuefeedbackapplication.parkingdetails;


import com.feedback.customer.vuefeedbackapplication.abstractclass.AbstractClass;
import com.feedback.customer.vuefeedbackapplication.category.Category;
import com.feedback.customer.vuefeedbackapplication.staff.Branch;
import com.feedback.customer.vuefeedbackapplication.user.Status;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table(name = "parking_details")
public class ParkingDetails extends AbstractClass<Long> {

    private String vehicleNumber;

    private LocalDateTime checkIn;

    private LocalDateTime checkOut;

    private String duration;

    private Double price;

    @OneToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "staff_id")
    private Branch branch;

    private Status status;

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public LocalDateTime getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(LocalDateTime checkIn) {
        this.checkIn = checkIn;
    }

    public LocalDateTime getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(LocalDateTime checkOut) {
        this.checkOut = checkOut;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
