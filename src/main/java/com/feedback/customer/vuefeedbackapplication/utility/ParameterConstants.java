package com.feedback.customer.vuefeedbackapplication.utility;

public class ParameterConstants {

    public static final String PARAM_ERROR="error";
    public static final String PARAM_USER_ERROR="userError";
    public static final String PARAM_MESSAGE="message";
    public static final String PARAM_TEXT="txt";
    public static final String PARAM_BRANCH="branch";
    public static final String PARAM_USER="user";
    public static final String PARAM_VALID="valid";
    public static final String PARAM_SHIFT_LIST="shift";
    public static final String PARAM_VEHICLE_TYPE="vehicleType";
    public static final String REMAINING_VEHICLE_TYPE="remainingType";
    public static final String PARAM_ANSWER_TYPE_LIST="answerTypeList";
    public static final String PARAM_CUSTOMER ="customer";
    public static final String PARAM_CUSTOMER_ANSWER ="tellerAnswer";
    public static final String PARAM_TELLER_LIST="tellerlist";
    public static final String PARAM_SIDEBAR="sidenavbar";
    public static final String PARAM_CHANGE_PASSWORD="changePassword";
    public static final String PARAM_ACTION="action";
    public static final String PARAM_INVOICES ="invoice";

    public static final String PARAM_CATEGORY="category";
    public static final String PARAM_QUESTION="questionlist";
    public static final String PARAM_SUB_ANSWER="subanswer";

    public static final String PARAM_ISSUES_LIST="issueList";
    public static final String PARAM_ISSUES_DETAIL="detail";

    public static final String PARAM_COMMENT_LIST="commentList";
    public static final String PARAM_COMMENT_ERROR="commentError";

    public static final String PARAM_FILE_PATH_SAVE="C:/tourism_feedback/";
    public static final String PARAM_LINUX_FILE_PATH_SAVE="/opt/tourism_feedback/";

    public static final String PARAM_TOTAL="total";
    public static final String PARAM_AVERAGE_PERCENTAGE="totalPercentage";

}