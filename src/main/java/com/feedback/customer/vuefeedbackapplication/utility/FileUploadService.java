package com.feedback.customer.vuefeedbackapplication.utility;


import org.apache.commons.lang3.SystemUtils;
import org.springframework.stereotype.Service;

@Service
public class FileUploadService {
    public String getBasePath() {
        if (SystemUtils.IS_OS_WINDOWS) {
            return ParameterConstants.PARAM_FILE_PATH_SAVE;
        } else if (SystemUtils.IS_OS_LINUX) {
            return ParameterConstants.PARAM_LINUX_FILE_PATH_SAVE;
        } else if (SystemUtils.IS_OS_MAC) {
            return ParameterConstants.PARAM_LINUX_FILE_PATH_SAVE;
        }
        return "";
    }
}
