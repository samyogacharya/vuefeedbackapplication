package com.feedback.customer.vuefeedbackapplication.customeranswer;

import com.feedback.customer.vuefeedbackapplication.customer.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerAnswerRepository extends JpaRepository<CustomerAnswer,Long> {

    @Query("from CustomerAnswer t where t.id=?1")
    CustomerAnswer findCustomerAnswerById(Long id);

    List<CustomerAnswer> findByCustomer(Customer customer);

    List<CustomerAnswer> findByBranch_Id(Long id);

}
