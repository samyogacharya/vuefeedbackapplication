package com.feedback.customer.vuefeedbackapplication.customeranswer;

import com.feedback.customer.vuefeedbackapplication.answer.AnswerDTO;
import com.feedback.customer.vuefeedbackapplication.answer.CommentDTO;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface CustomerAnswerService {

    CustomerAnswerDTO save(CustomerAnswerDTO customerAnswerDTO);

    List<CustomerAnswerDTO> findAll();

    List<CustomerAnswerDTO> findByCustomer(CustomerDTO customerDTO);

    List<CustomerAnswerDTO> findByBranch_Id(Long id);

    CustomerAnswerDTO findById(Long id);

    String fileName(MultipartFile file) throws IOException;

    AnswerDTO saveImages(CustomerAnswerDTO customerAnswerDTO);

    List<CommentDTO> convetToComment(CustomerAnswerDTO customerAnswerDTO);

}
