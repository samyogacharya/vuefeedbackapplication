package com.feedback.customer.vuefeedbackapplication.customeranswer;

import com.feedback.customer.vuefeedbackapplication.answer.AnswerConverter;
import com.feedback.customer.vuefeedbackapplication.answer.AnswerService;
import com.feedback.customer.vuefeedbackapplication.category.CategoryConverter;
import com.feedback.customer.vuefeedbackapplication.category.CategoryService;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerConverter;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerService;
import com.feedback.customer.vuefeedbackapplication.staff.BranchConverter;
import com.feedback.customer.vuefeedbackapplication.staff.BranchService;
import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.utility.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerAnswerConverter implements EntityMapper<CustomerAnswerDTO, CustomerAnswer> {

    @Autowired
    CustomerAnswerRepository customerAnswerRepository;

    @Autowired
    AnswerConverter answerConverter;

    @Autowired
    AnswerService answerService;

    @Autowired
    CustomerConverter customerConverter;

    @Autowired
    CustomerService customerService;

    @Autowired
    CategoryConverter categoryConverter;

    @Autowired
    CategoryService categoryService;

    @Autowired
    BranchService branchService;

    @Autowired
    BranchConverter branchConverter;

    @Override
    public CustomerAnswer toEntity(CustomerAnswerDTO dto) {
        if (dto == null) {
            return null;
        }
        CustomerAnswer entity;
        if (dto.getId() != null) {
            entity = customerAnswerRepository.findCustomerAnswerById(dto.getId());
        } else {
            entity = new CustomerAnswer();
            toEntity(dto, entity);
            entity.setStatus(Status.ACTIVE);
            entity.setAnswer(answerConverter.toEntity(answerService.findAnswerById(dto.getAnswerId())));
            entity.setCustomer(customerConverter.toEntity(customerService.findCustomerById(dto.getCustomerId())));
            entity.setCategory(categoryConverter.toEntity(categoryService.findCategoryById(dto.getCategoryId())));
            entity.setBranch(branchConverter.toEntity(branchService.findBranchById(dto.getBranchId())));
        }
        return entity;
    }

    @Override
    public CustomerAnswer toEntity(CustomerAnswerDTO dto, CustomerAnswer entity) {
        if (entity == null || dto == null) {
            return null;
        }
        return entity;
    }

    @Override
    public CustomerAnswerDTO toDto(CustomerAnswer entity) {
        if (entity == null) {
            return null;
        }
        CustomerAnswerDTO dto = new CustomerAnswerDTO();
        dto.setId(entity.getId());
        dto.setAnswerId(entity.getAnswer().getId());
        dto.setCustomerId(entity.getCustomer().getId());
        dto.setCategoryId(entity.getCategory().getId());
        dto.setAnswerDescription(entity.getAnswer().getDescription());
        dto.setBranchId(entity.getBranch().getId());
        dto.setBranchName(entity.getBranch().getBranchName());
        dto.setCustomerName(entity.getCustomer().getName());
        dto.setCategoryName(entity.getCategory().getCategoryName());
        dto.setAnswerDescription(entity.getAnswer().getDescription());
        dto.setAnswerTitle(entity.getAnswer().getTitle());

        dto.setRemarks(entity.getAnswer().getRemarks());
        dto.setRemarksDate(entity.getAnswer().getRemarksDate());
        dto.setRemarks2(entity.getAnswer().getRemarks2());
        dto.setRemarks2Date(entity.getAnswer().getRemarks2Date());

        dto.setAdminRemarks(entity.getAnswer().getAdminRemarks());
        dto.setAdminRemarksDate(entity.getAnswer().getAdminRemarksDate());
        dto.setAdminRemarks2(entity.getAnswer().getAdminRemarks2());
        dto.setAdminRemarks2Date(entity.getAnswer().getAdminRemarks2Date());

        dto.setSystemAdminRemarks(entity.getAnswer().getSystemAdminRemarks());
        dto.setSystemAdminRemarksDate(entity.getAnswer().getSystemAdminRemarksDate());
        dto.setSystemAdminRemarks2(entity.getAnswer().getSystemAdminRemarks2());
        dto.setSystemAdminRemarks2Date(entity.getAnswer().getSystemAdminRemarks2Date());

        dto.setInsertDate(entity.getCreated());

        dto.setImages(entity.getAnswer().getImages());

        return dto;
    }

    @Override
    public List<CustomerAnswer> toEntity(List<CustomerAnswerDTO> dtoList) {
        if (dtoList == null || dtoList.isEmpty()) {
            return null;
        }
        return dtoList.parallelStream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<CustomerAnswerDTO> toDto(List<CustomerAnswer> entityList) {
        if (entityList == null) {
            return null;
        }
        return entityList.parallelStream().map(this::toDto).collect(Collectors.toList());
    }
}
