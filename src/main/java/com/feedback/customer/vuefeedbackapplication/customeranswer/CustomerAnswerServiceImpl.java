package com.feedback.customer.vuefeedbackapplication.customeranswer;

import com.feedback.customer.vuefeedbackapplication.answer.AnswerDTO;
import com.feedback.customer.vuefeedbackapplication.answer.AnswerService;
import com.feedback.customer.vuefeedbackapplication.answer.CommentDTO;
import com.feedback.customer.vuefeedbackapplication.category.CategoryService;
import com.feedback.customer.vuefeedbackapplication.customer.Customer;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerConverter;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerDTO;
import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.utility.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerAnswerServiceImpl implements CustomerAnswerService {

    @Autowired
    CustomerAnswerRepository customerAnswerRepository;

    @Autowired
    CustomerAnswerConverter customerAnswerConverter;

    @Autowired
    CustomerConverter customerConverter;

    @Autowired
    AnswerService answerService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    FileUploadService fileUploadService;

    @Override
    public CustomerAnswerDTO save(CustomerAnswerDTO customerAnswerDTO) {

        AnswerDTO answerDTO = new AnswerDTO();
        answerDTO.setTitle(customerAnswerDTO.getAnswerTitle());
        answerDTO.setDescription(customerAnswerDTO.getAnswerDescription());
        answerDTO.setStatus(Status.ACTIVE);
        answerDTO = answerService.save(answerDTO);

        customerAnswerDTO.setAnswerId(answerDTO.getId());
        customerAnswerDTO.setStatus(Status.ACTIVE);
        CustomerAnswer customerAnswer = customerAnswerConverter.toEntity(customerAnswerDTO);
        customerAnswer = customerAnswerRepository.save(customerAnswer);
        return customerAnswerConverter.toDto(customerAnswer);
    }

    public AnswerDTO saveImages(CustomerAnswerDTO customerAnswerDTO){
        AnswerDTO answerDTO = answerService.findAnswerById(customerAnswerDTO.getAnswerId());
        answerDTO.setImages(customerAnswerDTO.getImages());
        return answerService.save(answerDTO);
    }

    @Override
    public List<CustomerAnswerDTO> findAll() {
        return customerAnswerConverter.toDto(customerAnswerRepository.findAll());
    }

    @Override
    public List<CustomerAnswerDTO> findByCustomer(CustomerDTO customerDTO) {
        Customer customer = customerConverter.toEntity(customerDTO);
        return customerAnswerConverter.toDto(customerAnswerRepository.findByCustomer(customer));
    }

    @Override
    public List<CustomerAnswerDTO> findByBranch_Id(Long id) {
        return customerAnswerConverter.toDto(customerAnswerRepository.findByBranch_Id(id));
    }

    @Override
    public CustomerAnswerDTO findById(Long id) {
        return customerAnswerConverter.toDto(customerAnswerRepository.findCustomerAnswerById(id));
    }

    @Override
    public String fileName(MultipartFile file) throws IOException {
        String basePath = fileUploadService.getBasePath();
        createDirectory(basePath+"complaints/");
        String uniqueName= String.valueOf(System.currentTimeMillis());
        File imgFile = new File(basePath+"complaints/"+uniqueName+file.getOriginalFilename());
        file.transferTo(imgFile);
        return uniqueName+file.getOriginalFilename();
    }

    private void createDirectory(String basePath) {
        File dir = new File(basePath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
    }

    @Override
    public List<CommentDTO> convetToComment(CustomerAnswerDTO customerAnswerDTO){
        List<CommentDTO> list = new ArrayList<>();

        if (customerAnswerDTO.getRemarks()!=null) {
            CommentDTO commentDTO = new CommentDTO();
            commentDTO.setComment(customerAnswerDTO.getRemarks());
            commentDTO.setCommentBy("Customer");
            commentDTO.setCommentedTime(convertDateToString(customerAnswerDTO.getRemarksDate()));
            list.add(commentDTO);
        }

        if (customerAnswerDTO.getRemarks2()!=null) {
            CommentDTO commentDTO1 = new CommentDTO();
            commentDTO1.setComment(customerAnswerDTO.getRemarks2());
            commentDTO1.setCommentBy("Customer");
            commentDTO1.setCommentedTime(convertDateToString(customerAnswerDTO.getRemarks2Date()));
            list.add(commentDTO1);
        }

        if (customerAnswerDTO.getAdminRemarks()!=null) {
            CommentDTO commentDTO2 = new CommentDTO();
            commentDTO2.setComment(customerAnswerDTO.getAdminRemarks());
            commentDTO2.setCommentBy("Admin");
            commentDTO2.setCommentedTime(convertDateToString(customerAnswerDTO.getAdminRemarksDate()));
            list.add(commentDTO2);
        }

        if (customerAnswerDTO.getAdminRemarks2()!=null) {
            CommentDTO commentDTO3 = new CommentDTO();
            commentDTO3.setComment(customerAnswerDTO.getAdminRemarks2());
            commentDTO3.setCommentBy("Admin");
            commentDTO3.setCommentedTime(convertDateToString(customerAnswerDTO.getAdminRemarks2Date()));
            list.add(commentDTO3);
        }

        if (customerAnswerDTO.getSystemAdminRemarks2()!=null) {
            CommentDTO commentDTO4 = new CommentDTO();
            commentDTO4.setComment(customerAnswerDTO.getSystemAdminRemarks2());
            commentDTO4.setCommentBy("SysAdmin");
            commentDTO4.setCommentedTime(convertDateToString(customerAnswerDTO.getSystemAdminRemarks2Date()));
            list.add(commentDTO4);
        }

        if (customerAnswerDTO.getSystemAdminRemarks()!=null) {
            CommentDTO commentDTO5 = new CommentDTO();
            commentDTO5.setComment(customerAnswerDTO.getSystemAdminRemarks());
            commentDTO5.setCommentBy("SysAdmin");
            commentDTO5.setCommentedTime(convertDateToString(customerAnswerDTO.getSystemAdminRemarksDate()));
            list.add(commentDTO5);
        }

        return list;
    }

    private String convertDateToString(LocalDateTime localDateTime){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, d MMM yyyy HH:mm a");
        String formattedDateTime = localDateTime.format(formatter);
        return formattedDateTime;
    }
}
