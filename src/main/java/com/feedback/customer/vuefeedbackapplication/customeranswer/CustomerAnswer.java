package com.feedback.customer.vuefeedbackapplication.customeranswer;

import com.feedback.customer.vuefeedbackapplication.abstractclass.AbstractClass;
import com.feedback.customer.vuefeedbackapplication.answer.Answer;
import com.feedback.customer.vuefeedbackapplication.category.Category;
import com.feedback.customer.vuefeedbackapplication.customer.Customer;
import com.feedback.customer.vuefeedbackapplication.staff.Branch;
import com.feedback.customer.vuefeedbackapplication.user.Status;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "customer_answer")
public class CustomerAnswer extends AbstractClass<Long> {

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "answer_id")
    private Answer answer;

    @ManyToOne
    @JoinColumn(name = "branch_id")
    private Branch branch;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    private String remarks;

    private Status status;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
