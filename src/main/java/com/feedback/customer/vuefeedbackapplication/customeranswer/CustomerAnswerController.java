package com.feedback.customer.vuefeedbackapplication.customeranswer;

import com.feedback.customer.vuefeedbackapplication.answer.CommentDTO;
import com.feedback.customer.vuefeedbackapplication.configuration.SecurityUtility;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerService;
import com.feedback.customer.vuefeedbackapplication.staff.BranchDTO;
import com.feedback.customer.vuefeedbackapplication.staff.BranchService;
import com.feedback.customer.vuefeedbackapplication.user.User;
import com.feedback.customer.vuefeedbackapplication.utility.FileUploadService;
import com.feedback.customer.vuefeedbackapplication.utility.ParameterConstants;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping(value = "/auth")
public class CustomerAnswerController {

    @Autowired
    SecurityUtility securityUtility;

    @Autowired
    CustomerAnswerService customerAnswerService;

    @Autowired
    CustomerService customerService;

    @Autowired
    BranchService branchService;

    @Autowired
    FileUploadService fileUploadService;

    @GetMapping("/customerAnswer/{id}")
    private String getCustomerAnswerByBranch(@PathVariable("id")Long id){
        List<CustomerAnswerDTO> answerDTOList = customerAnswerService.findByBranch_Id(id);
        return "";
    }

    @GetMapping("/customerissue/detail/{id}")
    private String getCustomerIssueDetail(@PathVariable("id")Long id, Model model){
        User user = securityUtility.getSecurity();
        CustomerAnswerDTO customerAnswerDTO = customerAnswerService.findById(id);

        List<CommentDTO> commentDTOList = customerAnswerService.convetToComment(customerAnswerDTO);
        commentDTOList.sort(Comparator.comparing(CommentDTO::getCommentedTime));

        model.addAttribute(ParameterConstants.PARAM_COMMENT_LIST,commentDTOList);
        model.addAttribute(ParameterConstants.PARAM_SIDEBAR,user.getUserType().name());
        model.addAttribute(ParameterConstants.PARAM_ISSUES_DETAIL,customerAnswerDTO);
        return "issues/detail";
    }

    @GetMapping("/admin/customerissue/{id}")
    private String getIssuesFromBranch(Model model, @PathVariable Long id){
        User user = securityUtility.getSecurity();
        BranchDTO branch = branchService.findBranchById(id);
        List<CustomerAnswerDTO> customerAnswerDTOList= customerAnswerService.findByBranch_Id(branch.getId());
        customerAnswerDTOList.sort(Comparator.comparing(CustomerAnswerDTO::getId).reversed());
        model.addAttribute(ParameterConstants.PARAM_ISSUES_LIST,customerAnswerDTOList);
        model.addAttribute(ParameterConstants.PARAM_SIDEBAR,user.getUserType().name());
        return "issues/list";
    }

    @GetMapping("/customerissue")
    private String getAllIssues(Model model){
        User user = securityUtility.getSecurity();
        BranchDTO branch = branchService.findByUserAndStatus(user);
        List<CustomerAnswerDTO> customerAnswerDTOList=null;
        if (user.getUserType().name().equalsIgnoreCase("SYSTEM_ADMIN")){
            customerAnswerDTOList = customerAnswerService.findAll();
        }else if (user.getUserType().name().equalsIgnoreCase("ADMIN")){
            customerAnswerDTOList = customerAnswerService.findByBranch_Id(branch.getId());
        }
        customerAnswerDTOList.sort(Comparator.comparing(CustomerAnswerDTO::getId).reversed());
        model.addAttribute(ParameterConstants.PARAM_ISSUES_LIST,customerAnswerDTOList);
        model.addAttribute(ParameterConstants.PARAM_SIDEBAR,user.getUserType().name());
        return "issues/list";
    }

    @RequestMapping(value = "/view/image", method = RequestMethod.GET)
    public void viewProjectTypeImage(@RequestParam("fileName") String fileName, HttpServletResponse response, HttpServletRequest request) throws IOException {
        if (!StringUtils.isEmpty(fileName)) {
            fileName = URLDecoder.decode(fileName, "UTF-8");

            File imagePath = new File(fileUploadService.getBasePath()+"complaints/"+fileName);
            if (imagePath.exists()) {
                String ext = FilenameUtils.getExtension(fileName);
                if (ext.equals("png") || ext.equals("jpg") || ext.equals("jpeg")) {
                    response.setContentType("image/" + ext);
                } else if (ext.equals("pdf")) {
                    response.setContentType("application/" + ext);
                }
                response.setHeader("Content-Disposition", "attachment;filename=" + fileName);

                FileCopyUtils.copy(new FileInputStream(imagePath), response.getOutputStream());

            }

        }

    }

}
