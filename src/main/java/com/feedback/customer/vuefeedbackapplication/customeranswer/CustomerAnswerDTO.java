package com.feedback.customer.vuefeedbackapplication.customeranswer;


import com.feedback.customer.vuefeedbackapplication.user.Status;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class CustomerAnswerDTO {

    private Long id;

    private Date insertDate;

    private Long answerId;

    private String answerTitle;

    private String answerDescription;

    private String remarks;

    private LocalDateTime remarksDate;

    private String remarks2;

    private LocalDateTime remarks2Date;

    private String adminRemarks;

    private LocalDateTime adminRemarksDate;

    private String adminRemarks2;

    private LocalDateTime adminRemarks2Date;

    private String systemAdminRemarks;

    private LocalDateTime systemAdminRemarksDate;

    private String systemAdminRemarks2;

    private LocalDateTime systemAdminRemarks2Date;

    private Long customerId;

    private String customerName;

    private Long branchId;

    private String branchName;

    private Long categoryId;

    private String categoryName;

    private String categoryNameNepali;

    private String categoryDescription;

    private String categoryDescriptionNepali;

    private Status status;

    private List<String> images;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Long answerId) {
        this.answerId = answerId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getAnswerTitle() {
        return answerTitle;
    }

    public void setAnswerTitle(String answerTitle) {
        this.answerTitle = answerTitle;
    }

    public String getAnswerDescription() {
        return answerDescription;
    }

    public void setAnswerDescription(String answerDescription) {
        this.answerDescription = answerDescription;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryNameNepali() {
        return categoryNameNepali;
    }

    public void setCategoryNameNepali(String categoryNameNepali) {
        this.categoryNameNepali = categoryNameNepali;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public String getCategoryDescriptionNepali() {
        return categoryDescriptionNepali;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public void setCategoryDescriptionNepali(String categoryDescriptionNepali) {
        this.categoryDescriptionNepali = categoryDescriptionNepali;
    }

    public LocalDateTime getRemarksDate() {
        return remarksDate;
    }

    public void setRemarksDate(LocalDateTime remarksDate) {
        this.remarksDate = remarksDate;
    }

    public String getRemarks2() {
        return remarks2;
    }

    public void setRemarks2(String remarks2) {
        this.remarks2 = remarks2;
    }

    public LocalDateTime getRemarks2Date() {
        return remarks2Date;
    }

    public void setRemarks2Date(LocalDateTime remarks2Date) {
        this.remarks2Date = remarks2Date;
    }

    public String getAdminRemarks() {
        return adminRemarks;
    }

    public void setAdminRemarks(String adminRemarks) {
        this.adminRemarks = adminRemarks;
    }

    public LocalDateTime getAdminRemarksDate() {
        return adminRemarksDate;
    }

    public void setAdminRemarksDate(LocalDateTime adminRemarksDate) {
        this.adminRemarksDate = adminRemarksDate;
    }

    public String getAdminRemarks2() {
        return adminRemarks2;
    }

    public void setAdminRemarks2(String adminRemarks2) {
        this.adminRemarks2 = adminRemarks2;
    }

    public LocalDateTime getAdminRemarks2Date() {
        return adminRemarks2Date;
    }

    public void setAdminRemarks2Date(LocalDateTime adminRemarks2Date) {
        this.adminRemarks2Date = adminRemarks2Date;
    }

    public String getSystemAdminRemarks() {
        return systemAdminRemarks;
    }

    public void setSystemAdminRemarks(String systemAdminRemarks) {
        this.systemAdminRemarks = systemAdminRemarks;
    }

    public LocalDateTime getSystemAdminRemarksDate() {
        return systemAdminRemarksDate;
    }

    public void setSystemAdminRemarksDate(LocalDateTime systemAdminRemarksDate) {
        this.systemAdminRemarksDate = systemAdminRemarksDate;
    }

    public String getSystemAdminRemarks2() {
        return systemAdminRemarks2;
    }

    public void setSystemAdminRemarks2(String systemAdminRemarks2) {
        this.systemAdminRemarks2 = systemAdminRemarks2;
    }

    public LocalDateTime getSystemAdminRemarks2Date() {
        return systemAdminRemarks2Date;
    }

    public void setSystemAdminRemarks2Date(LocalDateTime systemAdminRemarks2Date) {
        this.systemAdminRemarks2Date = systemAdminRemarks2Date;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }
}
