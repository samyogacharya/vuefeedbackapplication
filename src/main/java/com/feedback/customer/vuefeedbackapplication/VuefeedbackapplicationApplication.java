package com.feedback.customer.vuefeedbackapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VuefeedbackapplicationApplication {

    public static void main(String[] args) {
        SpringApplication.run(VuefeedbackapplicationApplication.class, args);
    }

}
