package com.feedback.customer.vuefeedbackapplication.configuration;

import com.feedback.customer.vuefeedbackapplication.customer.Customer;
import com.feedback.customer.vuefeedbackapplication.customer.CustomerRepository;
import com.feedback.customer.vuefeedbackapplication.staff.Branch;
import com.feedback.customer.vuefeedbackapplication.staff.BranchRepository;
import com.feedback.customer.vuefeedbackapplication.user.Status;
import com.feedback.customer.vuefeedbackapplication.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurityUtility {

    @Autowired
    BranchRepository branchRepository;

    @Autowired
    CustomerRepository customerRepository;

//    public User getCurrentUser() {
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        User user = (User) auth.getPrincipal();
//        return user;
//    }

    public User getSecurity() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object principal = auth.getPrincipal();
        if (principal instanceof CustomUserDetails) {
            return ((CustomUserDetails) principal).getUser();
        }
        return null;
    }

    public Branch getCurrentBranch() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object principal = auth.getPrincipal();
        if (principal instanceof CustomUserDetails) {
            User user=((CustomUserDetails) principal).getUser();
            Branch branch = branchRepository.findByUserAndStatus(user, Status.ACTIVE);
            return branch;
        }
        return null;
    }

    public Customer getCurrentTeller() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object principal = auth.getPrincipal();
        if (principal instanceof CustomUserDetails) {
            User user=((CustomUserDetails) principal).getUser();
            Customer customer = customerRepository.findByUserAndStatus(user, Status.ACTIVE);
            return customer;
        }
        return null;
    }

}
