package com.feedback.customer.vuefeedbackapplication.configuration;//package com.feedback.customer.configuration;

import com.feedback.customer.vuefeedbackapplication.VuefeedbackapplicationApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(VuefeedbackapplicationApplication.class);
    }

}
