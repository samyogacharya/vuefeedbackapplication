import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Protected from '@/components/Protected'
import store from './store'
import Staff from '@/components/Staff';
import Category from "./components/Category";
import Invoice from "./components/Invoice";
import Setting from "./components/Setting";
import SuperAdmin from "./components/SuperAdmin";
import Admin from "./components/Admin";
import InvoiceById from "./components/InvoiceById";

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [
        { path: '/login', component: Login },
        {
            path: '/protected',
            component: Protected,
            meta: {
                requiresAuth: true
            }
        },
        {path:'/staff', component: Staff},
        {path:'/Category', component: Category},
        {path:'/Invoice', component: Invoice},
        {path:'/Setting', component: Setting},
        {path: '/SuperAdmin', component: SuperAdmin},
        {path:'/Admin', component: Admin},
        {path:'/InvoiceById', component: InvoiceById}
    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!store.getters.isLoggedIn) {
            next({
                path: '/login'
            })
        } else {
            next();
        }
    } else {
        next(); // make sure to always call next()!
    }
});

export default router;