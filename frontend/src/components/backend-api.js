import axios from 'axios'

const AXIOS = axios.create({
    baseURL: `/auth`
});
export default {
    props: {
        BranchUserDTO: {
            'id': '',
            'branchName': '',
            'shift': '',
            'email': '',
            'address': '',
            'status': '',
            'userId': '',
            'userLogin': '',
            'username': '',
            'password': ''
        },
        retrievedStaff: {
            BranchUserDTO: {
                'id': '',
                'branchName': '',
                'shift': '',
                'address': '',
                'userId': '',
                'username': '',
                'password': '',
                'email': ''
            }
        },
        retrieveBranch:{},
        ChangePasswordDTO:{'oldPassword':'', 'newPassword':'', 'confirmPassword':''},
        retrieveCategory:{},
        FilterEnum:{'filterType':''},
        username:'',
        password:'',
        CategoryDTO:{'id':'', 'categoryName':'', 'categoryNameNepali':'', 'rate':'', 'categoryDescription':'', 'categoryDescriptionNepali':'', 'vehicleType':''}
    },
    getSecured(user, password) {
        return AXIOS.get(`/`, {
            auth: {
                username: user,
                password: password
            }
        });
    },
    getInvoiceByDate(FilterEnum)
    {
        return AXIOS.get('/admin/user/List/'+FilterEnum);
    },
    getInvoiceById(userId)
    {
        console.log(userId)
        return AXIOS.get('/admin/invoices/listById/'+userId+'/'+'DAILY');
    },
    getListUser(FilterEnum)
    {
        return AXIOS.get('/admin/user/List/' +FilterEnum);
    },
    findIndividualBranch(userId)
    {
        console.log(userId);
        return AXIOS.get('/admin/invoices/branch/'+userId);
    },
    getIndividualData(id)
    {
        return AXIOS.get('/admin/invoices/getIndividualData/'+id)
    },
    getInvoiceIdByFilter(userId,FilterEnum) {
        return AXIOS.get('/admin/invoices/listById/'+userId+'/'+FilterEnum);
    },
    createExcel()
    {
        return AXIOS.get('/admin/createExcel/');
    },
    saveStaff(BranchUserDTO) {
        console.log(BranchUserDTO);
        return AXIOS.post('/admin/staff/add', BranchUserDTO);
    },
    getStaffById(id) {
        console.log(id);
        return AXIOS.get('/admin/staff/edit/' + id);
    },
    updateStaff(retrieveBranch)
    {
        return AXIOS.post('/admin/staff/edit',retrieveBranch);
    },
    saveCategory(CategoryDTO)
    {
        return AXIOS.post('/admin/category/add', CategoryDTO);
    },
    getCategoryById(id)
    {
        console.log(id);
        return AXIOS.get('/admin/category/edit/'+id);
    },
    updateCategory(retrieveCategory)
    {
        console.log(retrieveCategory)
        return AXIOS.post('/admin/category/edit', retrieveCategory);
    },
    logout()
    {
        return AXIOS.get('/logout-success')
    },
    changePassword(ChangePasswordDTO)
    {
        return AXIOS.post('/changePassword', ChangePasswordDTO);
    },
    getBranchDashboard()
    {
        return AXIOS.get('/dashboard/');
    }
}

