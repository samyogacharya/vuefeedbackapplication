import Sidebar from './components/Sidebar.vue'
import DataTable from './components/DataTable.vue'
import InvoiceById  from "./components/InvoiceById";

export default {
  install (Vue) {
    Vue.component('sidebar-menu', Sidebar)
    Vue.component('datatable', DataTable)
    Vue.component('invoiceById', InvoiceById)
  }
}

export { Sidebar, DataTable }
